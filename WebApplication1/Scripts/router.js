define(['backbone', 'common'], function (Backbone, cm) {
	var AppRouter = Backbone.Router.extend({
		routes : {
			'' : 'UserList',
			'HelloWorld':'HelloWorldFun',
			'HiWorld/':'HiWorldFun', //注意斜線
			'HiWorld':'HiWorldFun',
			'HelloDefine':'HelloDefineFun',
			'HiDefine':'HiDefineFun',
			'User':'UserList',			
			'User/id=:id':'UserSingle',			
			'logout':'logout'            
		},
		initialize:function(){
			console.log("Backbone.Router.extend.initialize");
		},
        execute:function(callback, args, name) {
			console.log("Backbone.Router.extend.execute:",name);
			if (callback) callback.apply(this, args);
		},
		HelloDefineFun:function()
		{
			require(['Commponent/HelloDefine/HelloWorld'], function (commponent) {
				const Main = React.createFactory(commponent.Main);	
				React.render(Main(), $("body")[0]);
			});
		},	
		HiDefineFun:function()
		{
			require(['Commponent/HelloDefine/HiWorld'], function (commponent) {
				const Main = React.createFactory(commponent.Main);	
				React.render(Main(), $("body")[0]);
			});
		},		
		HelloWorldFun:function()
		{
			require(['Commponent/Hello/HelloWorld'], function (commponent) {
				const Main = React.createFactory(commponent);	
				React.render(Main(), $("body")[0]);
			});
		},	
		HiWorldFun:function()
		{
			require(['Commponent/Hello/HiWorld'], function (commponent) {
				const Main = React.createFactory(commponent.Main);	
				React.render(Main(), $("body")[0]);
			});
		},		
		UserList:function()
		{
			require(['Commponent/User/UserList'], function (commponent) {
				const Main = React.createFactory(commponent);	
				React.render(Main(), $("body")[0]);
			});
		},	
		UserSingle:function(id)
		{
			require(['Commponent/User/UserSingle'], function (commponent) {
				const Main = React.createFactory(commponent);	
				React.render(Main({id:id}), $("body")[0]);
			});
		},				
		logout: function (path) {
			console.log(path);
			window.location.href = 'Default';
		},
		navigateAndSaveLast:function(url,arg)
		{
			this.lastUrl=Backbone.history.fragment;
			this.navigate(url,arg);
		}	
	});
	
	//return new AppRouter();
	var r= new AppRouter();

	//Backbone.history.start();
    return r;
});
