define(['backbone'], function(Backbone) {

	var SyncServer = function(url,data,callback,IsAsync,errorcallback,type,useCache,token,contentType)
	{
	    if(IsAsync==null) IsAsync=true;
		if(type==null) type="POST";
		if(useCache==null) useCache=false;
		
		let headers = { uid: window['uid'] ? window['uid'] : '', fid: window['fid'] ? window['fid'] : '', CustId: window['_ID'] ? window['_ID'] : ''};
		if(token)
		{
			if(typeof token === 'string')
			{
				headers["Authorization"]="Bearer " + token;
			}else{
				headers = Object.assign(headers, token);
			}
		}
		if(token) token = "Bearer " + token;
		
		typeof "" === 'string'
		Backbone.ajax({
				dataType: "json",
				url: url,
				data: data,
				cache:useCache,
            	headers: headers,
				async:IsAsync,
				type:type,
				contentType:contentType,
				success: function(val)
				{
					DoPreloadDesc(url, "success");
					switch(val.ErrorCode)
					{

							case 500:
							case 100:							
							case 102:
							case 103:
							
								let reloadCount = 3;
								if(sessionStorage )
								{
									if(sessionStorage.getItem('reloadCount')==null)
									{
										reloadCount=0;
									}else{
										reloadCount = parseInt(sessionStorage.getItem('reloadCount'));
									}
									
								}
								
							
								if(reloadCount<3)
								{
									reloadCount++;
									if(sessionStorage ) sessionStorage.setItem('reloadCount',""+reloadCount);
									window.location.reload();
								}else{
									if(sessionStorage ) sessionStorage.setItem('reloadCount','0');
									window.location.href="Message/index?msg="+val.ErrorCode;
								}
										
										//
								/*
								require(['accountModel'], function(accountModel)
								{

									if(accountModel.get("LoginStatus"))
									{
										//loginModel.set({LoginStatus:false});
										if(val.ErrorCode==101){
											var parm=val.ErrorMsg.split(',');
											window.location.href="Message/IsUM?F="+parm[0]+"&T="+parm[1];
										}else
											window.location.href="Message/index?msg="+val.ErrorCode;
									}
									//loginModel.set({LoginStatus:false});
								});
								*/
								break;
							case 106:
								window.location.href="Message/index?msg="+val.ErrorCode;
								break;
							case 101:
								var parm=val.ErrorMsg.split(',');
								window.location.href="Message/IsUM?F="+parm[0]+"&T="+parm[1];
								break;
							case 104:
								window.location.href="Message/IPBlock";
								break;
							case 105:
								window.location.href="Message/DenyIPSG";
								break;
							case 107:
								if(window["_IsLicenseeAPI"]){
									alert(languageHelper.Get("lbl_StatusChanged"));
									require(['router'], function (router) {
										router.navigate("logout", { trigger: true });
									})
								}else{
									alert(languageHelper.Get("lbl_NotifyHasDeposit"));
									window.location.href="Login/Relogin";
									/*
									SyncServer("Login/Relogin", null, function(val){
										console.log("val.ErrorCode:"+val.ErrorCode);
										if(val.ErrorCode==1){
											window.location.href="logout?Auto=false";
										}else if(val.ErrorCode==9527){//auto login
											alert(languageHelper.Get("lbl_NotifyHasDeposit"));
											window.location.href="/";
										}else{
											window.location.href="logout?Auto=false";
										}
									}.bind(this), false, $ );
									*/
								}

								break;
						case 2001:
							alert(val.ErrorMsg);
							location.reload(true);
							break;
						case 3001:		
							window.location.href="Message/LicMessage?Message="+ val.ErrorMsg +"&errorcode="+val.ErrorCode;
							break;
						default:
							//console.log('val:'+val);							
							if(callback!=null)callback(val);
							break;
					}

				},
				error: function(data){
					console.log(data);
					DoPreloadDesc(url, "fail");
					(/.updateodds|.showallodds|.getcontributor/i).test(url) && errorcallback && errorcallback(data,status);
					// (/.updateodds|.showallodds/i).test(url) && errorcallback && errorcallback();
					if(data.responseText){
						if(data.responseText.indexOf('window.parent.location.href')>0){
							data.responseText=data.responseText.replace("<script>","").replace("</script>","");
							eval(data.responseText);
						}
					}
				}
		});




	}

	var SyncServerHtml = function(url, data, callback, IsAsync, errorcallback)
	{
	    if(IsAsync==null) IsAsync=true;
		Backbone.ajax({
				dataType: "html",
				url: url,
				data: data,
				cache:false,
				async:IsAsync,
				success: function(val)
				{

					if(val.indexOf("ErrorCode") > -1){
						var Message=JSON.parse(val);
						switch(Message.ErrorCode)
						{
							case 101:
								window.location.href ="Message/IsUM?msg="+Message.ErrorMsg;
								break;
							default:
								window.location.href="Message/index?msg="+Message.ErrorCode;
								break;
						}
					}

					if(callback!=null)callback(val);


				},
				error: function(data){
					console.log(data);
					if(data.status==500){
						window.location.href="Message/index?msg=500";
					}
				}
		});
	}

	var DoPreloadDesc = function (url, type) {
		if (!!$("#preloaderNew").length) {
			let no = 0;
			if ((/.GetAccountInfo/i).test(url)) {
				no = 1;
			} else if ((/.GetBannerInfo/i).test(url)) {
				no = 2;
			} else if ((/AppDownloadCheck./i).test(url)) {
				no = 3;
			} else if ((/.GetJSResource/i).test(url)) {
				no = 4;
			} else if ((/Favorites/i).test(url)) {
				no = 5;
			} else if ((/.GetSpreadBetTypeGroup/i).test(url)) {
				no = 6;
			} else if ((/.GetPeakHourSpread/i).test(url)) {
				no = 7;
			} else if ((/.GetUMInfo/.test(url))) {
				no = 8;
			} else if ((/.GetContributor/i).test(url)) {
				no = 9;
			} else if ((/CashOut./i).test(url)) {
				no = 10;
			}

			if (no != 0) $("#preloaderNew .l-gc-loading-page-icon-" + no).attr("data-status", type);
			if (type == "fail") {
				$("#preloaderNew .l-gc-loading-page__footer").attr("data-content", "alert");
				document.getElementById("lp3").setAttribute("style", "display:none");
				document.getElementById("lp4").removeAttribute("style");
			}
		}
	}

	function getCookie(name)
	{
		var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
		if(arr != null) return unescape(arr[2]); return null;

	}
	function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
	}
	function setDomainCookie(key, value, exdays) {
	    var d = new Date();
	    var exps = exdays || 365;
	    d.setTime(d.getTime() + (exps * 24 * 60 * 60 * 1000));
	    var expires = "expires=" + d.toGMTString();
	    var myDomain = CheckIPDomainType(window._host) ? window._host : "." + window._host;
	    var command = key + "=" + value + "; " + expires + "; path=/; domain=" + myDomain;
	    document.cookie = command;
	}
	function delDomainCookie(key) {
	    var d = new Date();
	    var expires = "expires=" + d.toGMTString();
	    var myDomain = CheckIPDomainType(window._host) ? window._host : "." + window._host;
	    var command = key + "=; " + expires + "; path=/; domain=" + myDomain;
	    document.cookie = command;
	}
	function SaveGesture(gesture, callback) {
	    var gestureCFS = "";
	    SyncServer("Default/GetPublicKey", null, function (val) {
	        if (val.Data.MKEY && val.Data.EKEY) {
	            require(['CfsEncodeFunc', 'RSAProvider'], function (CfsEncodeFunc, rsaProvider) {
	                gestureCFS = CfsEncodeFunc.MD5(gesture);
	                var provider = new rsaProvider.RSACrypt(val.Data.EKEY, val.Data.MKEY, 1024);
	                var enCode = provider.encryptedString(gestureCFS);
	                $("#loading").show();
	                var data = {
	                    Gesture: enCode
	                };

	                SyncServer("setting/SaveGesture", data, function (val) {
	                    if (val.Data.Code == 1) {
	                        $("#loading").hide();
	                        var rememberMe = getCookie("rememberMe");
	                        var accMd = require('accountModel');
	                        if (rememberMe != "true") {
	                            setDomainCookie("rememberMe", true, 365); //1�~�L��
	                        }
	                        var uName = "";
	                        var nName = "";
	                        var lName = "";
	                        var gesNames = getCookie("ges"); //utility.Storage.get("gesNames");
	                        var arrGesName = [];
	                        if (gesNames) {
	                            arrGesName = gesNames.split(",");
	                        }
	                        if (_SiteMode == 0) {
	                            uName = accMd.get("Username").toLowerCase();
	                            var arrIdx = arrGesName.indexOf(uName);
	                            if (arrIdx >= 0) {
	                                arrGesName.splice(arrIdx, 1);
	                            }
	                            arrGesName.unshift(uName);
	                            if (accMd.get("Nickname")) {
	                                nName = accMd.get("Nickname").replace("cn88$", "").toLowerCase();
	                                var arrIdx = arrGesName.indexOf(nName);
	                                if (arrIdx >= 0) {
	                                    arrGesName.splice(arrIdx, 1);
	                                }
	                                arrGesName.unshift(nName);
	                            }
	                            lName = accMd.get("Lname").replace("cn88$", "").toLowerCase();
	                        }
	                        else {
	                            lName = accMd.get("Lname").toLowerCase();
	                            var arrIdx = arrGesName.indexOf(lName);
	                            if (arrIdx >= 0) {
	                                arrGesName.splice(arrIdx, 1);
	                            }
	                            arrGesName.unshift(lName);
	                        }
	                        if (arrGesName.length > 10) arrGesName.length = 10;
	                        setDomainCookie("ges", arrGesName.join(","));

	                        var users = getCookie("UserName");
	                        if (users && users != "") {
	                            users = users.split(",");
	                        }
	                        else {
	                            users = [];
	                        }
	                        var uIdx = users.indexOf(lName);
	                        if (uIdx >= 0) {
	                            users.splice(uIdx, 1);
	                        }
	                        users.unshift(lName);
	                        setDomainCookie("UserName", users.join(","), 365); //1�~�L��
	                    }

	                    if (typeof callback === "function") {
	                        callback(val);
	                    }
	                }, false);
	            });
	        }
	    });
	}

	var CasinoLangPath=function(lan)
	{
		switch (lan.toLowerCase())
        {
            case "zh-tw":
                Reflanguage = "ch";
                break;
            case "zh-cn":
			case "zh-chs":
                Reflanguage = "cs";
                break;
			case "th-th":
                Reflanguage = "th";
                break;
            case "vi-vn":
                Reflanguage = "vn";
                break;
            case "id-id":
                Reflanguage = "id";
                break;
			case "ko-kr":
                Reflanguage = "ko";
				break;
		    case "ja-jp":
		        Reflanguage = "jp";
		        break;
		    case "hi-in":
		        Reflanguage = "hi";
		        break;
			case "my-mm":
				Reflanguage = "mm";
				break;
            default:
                Reflanguage = "en";
                break;
        }
		return Reflanguage;
	}

	var CultureToRef = function(lan){
		switch (lan.toLowerCase())
        {
            case "zh-tw":
                Reflanguage = "ch";
                break;
            case "zh-cn":
                Reflanguage = "cs";
                break;
            case "zh-chs":
                Reflanguage = "zhcn";
                break;
            case "th-th":
                Reflanguage = "th";
                break;
            case "vi-vn":
                Reflanguage = "vn";
                break;
            case "id-id":
                Reflanguage = "id";
                break;
			case "ko-kr":
                Reflanguage = "ko";
				break;
		    case "ja-jp":
		        Reflanguage = "jp";
		        break;
		    case "hi-in":
		        Reflanguage = "hi";
		        break;
			case "my-mm":
				Reflanguage = "mm";
				break;	
            default:
                Reflanguage = "en";
                break;
        }
		return Reflanguage;
	}

	var LanguageHelper = Backbone.Model.extend({
		defaults: {
			Lang:null
		},
		Data:null,
		initialize:function(){
			var lang = getCookie("_Mculture");
			lang = lang==null?'en-US':lang;
			//this.updateData(lang==null?'en-US':lang);

			this.set({Lang:lang});
			this.on("change:Lang",this.updateData.bind(this));
		},
		updateData:function(){
			var _self=this;

			var load = function(JS)
			{
				_self.Data=JS;
				if(BettypeName.IsReady)BettypeName.Update(lang);
				//EventHub.trigger('bettypeNameUpDate',lang);
				//EventHub.trigger('LoadLangComplete');
			};

			var lang = _self.get("Lang");
			var url="JSResourceApi/GetJSResource";
			if(window._isBefore)
				url="../JSResourceApi/GetJSResource";

			SyncServer(url, { lang: lang, version: encodeURIComponent(require.s.contexts._.config.urlArgs), skinMode: window._SkinMode }, function (val) {
			    //_self.Data = val.Data;
				load(val.Data);

			},false,null,"GET",true);

			//url,data,callback,IsAsync,errorcallback,type,useCache
			/*switch(lang)
			{
				case "en-US":
					require(['JSResource.en-US'],load);
					break;
				case "es-ES":
					require(['JSResource.es-ES'],load);
					break;
				case "fr-FR":
					require(['JSResource.fr-FR'],load);
					break;
				case "id-ID":
					require(['JSResource.id-ID'],load);
					break;
				case "it-IT":
					require(['JSResource.it-IT'],load);
					break;
				case "ja-JP":
					require(['JSResource.ja-JP'],load);
					break;
				case "ko-KR":
					require(['JSResource.ko-KR'],load);
					break;
				case "pt-PT":
					require(['JSResource.pt-PT'],load);
					break;
				case "ru-RU":
					require(['JSResource.ru-RU'],load);
					break;
				case "th-TH":
					require(['JSResource.th-TH'],load);
					break;
				case "vi-VN":
					require(['JSResource.vi-VN'],load);
					break;
				case "zh-CN":
				case "zh-CHS":
					require(['JSResource.zh-CN'],load);
					break;
				case "zh-TW":
					require(['JSResource.zh-TW'],load);
					break;
			    case "hi-IN":
			        require(['JSResource.hi-IN'], load);
			        break;
				default :
			        require(['JSResource.en-US'], load);
			        break;
			}*/
		},
		Get: function (code) {
		    if(this.Data==null)
			{
			   this.updateData();
			}

			return this.Data[code]?this.Data[code]:"";

		}
	});

	String.prototype.format = function (args) {
	    var singleParameter = true;
	    if (typeof args == "object") {
	        singleParameter = false;
	    }
		var str = this;
		return str.replace(String.prototype.format.regex, function(item) {
			var intVal = parseInt(item.substring(1, item.length - 1));
			var replace;
			if (intVal >= 0) {
			    if (singleParameter === true) {
			        replace = args;
			    }
			    else {
			        replace = args[intVal];
			    }
			} else if (intVal === -1) {
				replace = "{";
			} else if (intVal === -2) {
				replace = "}";
			} else {
				replace = "";
			}

			if (singleParameter === true && !isNaN(replace)) {
			    return Number(replace);
			}
			else {
			    return replace;
			}
		});
	};
	String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

    //bily : 去除字串開頭結尾特殊字元 ex: str.trimText([';' ,'.' ,' '])
    String.prototype.trimText = function (parm) {
        return this.trimTextStart(parm).trimTextEnd(parm);
    };

    //bily : 去除字串開頭特殊字元
    String.prototype.trimTextStart = function (parm) {
        var str = this.valueOf();

        if (Array.isArray(parm) && parm.length > 0) {
            while (parm.some(function (value) {
                return str.indexOf(value) === 0;
            })) {

                for (var j = 0; j < parm.length; j++) {
                    trims(parm[j]);
                }
            }

            return str;
        }
        else if (typeof (parm) == "string" && parm.length > 0) {
            return trims(parm);
        }
        else {
            return trims(" ");
        }

        function trims(pa) {
            if (pa !== "") {
                while (str.substr(0, pa.length) === pa) {
                    str = str.substr(pa.length);
                }
            }
            return str;
        }
    };

    //bily : 去除字串結尾特殊字元
    String.prototype.trimTextEnd = function (parm) {
        var str = this.valueOf();

        if (Array.isArray(parm) && parm.length > 0) {
            while (parm.some(function (value) {
                return str.substr(-value.length) === value;
            })) {

                for (var j = 0; j < parm.length; j++) {
                    trims(parm[j]);
                }
            }

            return str;
        }
        else if (typeof (parm) == "string" && parm.length > 0) {
            return trims(parm);
        }
        else {
            return trims(" ");
        }

        function trims(pa) {
            if (pa !== "") {
                while (str.substr(-pa.length) === pa) {
                    str = str.slice(0, -pa.length);
                }
            }
            return str;
        }
    };

    //bily : 特殊字串轉日期 ex: str.toFormat('MM/dd/yyyy HH:mm:ss')
    String.prototype.toDate = function (format) {

        if (format) {
            var val = this.valueOf().replace(/[^\d]/g, '');
            var ary = [];

            pattern = "(yyyy|m{1,2}|M{1,2}|d{1,2}|H{1,2}|s{1,2})";
            reg = new RegExp(pattern, "g");
            var ary2 = format.match(reg);

            if (val.length == format.replace(/[^YyHMmds]/g, '').length) {
                let len = 0;

                for (let a = 0; a < ary2.length; a++) {
                    ary[a] = val.substr(len, ary2[a].length);
                    len += ary2[a].length;
                }
            }

            var year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;

            if (ary.length > 0) {
                for (var i = 0; i < ary.length; i++) {
                    var str = ary[i];
                    var fmat = ary2[i];

                    if (fmat == "yyyy")
                        year = parseInt(str);
                    else if (fmat == "MM" || fmat == "M")
                        month = parseInt(str);
                    else if (fmat == "dd" || fmat == "d")
                        day = parseInt(str);
                    else if (fmat == "HH" || fmat == "H")
                        hour = parseInt(str);
                    else if (fmat == "mm" || fmat == "m")
                        min = parseInt(str);
                    else if (fmat == "ss" || fmat == "s")
                        sec = parseInt(str);

                }

                return new Date(year, month - 1, day, hour, min, sec);
            }
        }
        else {
            var date = new Date(this.valueOf().replace(/-/g, "/"));
            return date.toString() == "Invalid Date" ? null : date;
        }
        return null;

    };

    //bily :日期格式化 ex: date.toFormat('yyyy/MM/dd HH:mm:ss')
    Date.prototype.toFormat = function (format) {
        var pattern = "\\b(yyyy|yy|m{1,2}|M{1,2}|d{1,2}|H{1,2}|h{1,2}|s{1,2}|tt)\\b";
        var reg = new RegExp(pattern, "g");
        var ary = format.match(reg);

        if (ary != null) {
            for (var i = 0; i < ary.length; i++) {
                var str = ary[i];

                if (str == "yyyy")
                    format = format.replace(/yyyy/g, this.getFullYear().toString());
                else if (str == "yy")
                    format = format.replace(/yy/g, (this.getFullYear() % 100).toString());
                else if (str == "MM")
                    format = format.replace(/MM/g, ("0" + (this.getMonth() + 1)).substr(-2));
                else if (str == "M")
                    format = format.replace(/M/g, (this.getMonth() + 1).toString());
                else if (str == "dd")
                    format = format.replace(/dd/g, ("0" + this.getDate()).substr(-2));
                else if (str == "d")
                    format = format.replace(/d/g, this.getDate().toString());
                else if (str == "HH")
                    format = format.replace(/HH/g, ("0" + this.getHours()).substr(-2));
                else if (str == "H")
                    format = format.replace(/H/g, this.getHours().toString());
                else if (str == "hh")
                    format = format.replace(/hh/g, ("0" + (this.getHours() % 12 == 0 ? 12 : this.getHours() % 12)).substr(-2));
                else if (str == "h")
                    format = format.replace(/h/g, (this.getHours() % 12 == 0 ? 12 : this.getHours() % 12).toString());
                else if (str == "mm")
                    format = format.replace(/mm/g, ("0" + this.getMinutes()).substr(-2));
                else if (str == "m")
                    format = format.replace(/m/g, this.getMinutes().toString());
                else if (str == "ss")
                    format = format.replace(/ss/g, ("0" + this.getSeconds()).substr(-2));
                else if (str == "s")
                    format = format.replace(/s/g, this.getSeconds().toString());
                else if (str == "tt")
                    format = format.replace(/tt/g, this.getHours() < 12 ? "AM" : "PM");
            }
        }

        return format;
    };

    //bily : 數值格式化 ex: num.toFormat('#,0.00')
    Number.prototype.toFormat = function (format) {

        var str = this.toString();

        if (/^[.,#0]+$/g.test(format)) {
            var index = format.replace(/,/g, "").indexOf(".");/*格式化字串中小數點的index*/
            var index2 = str.indexOf("."); /*小數點的index*/
            var isThousand = format.indexOf(",") > 0 && format.substr(-1) != ","; /*是否有千分號 ,且千分號不在開頭及結尾位置*/
            format = format.replace(/[.,]/g, "");

            if (index > -1 || index2 > -1) { /*處理浮點數的部分*/
                var a = format.substr(index); /*格式化的小數部分*/
                var b = index2 == -1 ? 0 : str.substr(index2 + 1).length; /*數值的小數位數 */
                var c = a.length;  /*格式化的小數位數 */
                if (index == -1)
                    str = this.toFixed(0);
                else if (b >= c)
                    str = this.toFixed(c);
                else if (a.trimTextEnd("#").length >= b)
                    str = this.toFixed(a.trimTextEnd("#").length);
                else
                    str = this.toFixed(b);
            }

            var ary = str.match(/\d+/g); /* str.match(/\d+(?=\.)/g); */
            var s1 = ary[0];  /* 整數 */
            var s2 = ary[1];  /* 浮點數 */

            /* 處理整數的部分 */
            var x = format.substring(0, index < 0 ? format.length : index).trimTextStart("#").length;

            while (s1.length < x) {
                s1 = "0" + s1;
            }

            if (isThousand)
                s1 = s1.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            str = (this.valueOf() < 0 ? "-" : "") + s1 + (s2 == null ? "" : "." + s2);
        }

        return str;
    };

	var RefName = function()
	{
	  var RawData={};
	  this.New=function(data)
	  {
		 if(data !=null) RawData = data;
	  }
	  this.Update=function(data)
	  {
		for(var key in data)
		{
			RawData[key]=data[key];
		}
	  }
	  this.Remove=function(id)
	  {
		  delete RawData[id+""];
	  }
	  this.GetLogo=function(id)
	  {
		return  this.GetRawData(id,true);
	  }
	  
	  this.GetRawData=function(id,IsLogo=false)
	  {
		  var key =id+"";
		  if(RawData[key]==null) return "";
		  var tmp = RawData[key].split('|');
		  if(IsLogo)
		  {
			  if(tmp.length>1) return tmp[1];
			  return "0";
		  }
		  return tmp[0];
	  }
	  
	  this.GetName=function(id , IsNeutral)
	  {		 
		 var strNeutral="";
		 if(IsNeutral) strNeutral = languageHelper.Get("Neutral_Team");
		 var name = this.GetRawData(id);		
		 return name.length>0? name+strNeutral:name;
	  }
	}

	var bettypeName=function()
	{
		var RawData=null;
		this.IsReady=false;
		this.Get=function(id)
		{
			if(RawData==null)
			{
				this.Update(languageHelper.get("Lang"));
				//EventHub.on('bettypeNameUpDate',this.Update);
			}
		   return RawData[id];
		},
		this.Update=function(lang)
		{
		    var _self=this;
			var acc = require('accountModel');
			var url = (acc.get("IsBF")? "../":"") +'GetReferenceData/GetBettypeName';
			Backbone.ajax({
				dataType: "json",
				url: url,
				data: {lang:lang},
				async:false,
				success: function(val)
				{
					RawData = val;
					_self.IsReady=true;
				},
				error: function(data){
					console.log(data);
				}
			});
		}
	}

	var util=function()
	{
		this.QueryTopZIndex=function(select)
		{
			var max =-99999;
			$(select).each(function() {
			    if($( this ).css('zIndex')>max)
				{
					max=$( this ).css('zIndex');
				}
			});
			return max;
		}

		this.OddsTypeName=function(id)
		{
			var n="";
			switch(id)
			{
				case 2:
					n=languageHelper.Get("lbl_OddsTypeMin"+id);
					if(window._SiteMode==2){
						n=languageHelper.Get("China_Odds");
					}
					break;
				case 1:
				case 3:
				case 4:
				case 5:
				case 6:
					n=languageHelper.Get("lbl_OddsTypeMin"+id);
					break;
			}
			return n;
		}

		this.Storage ={
			IsAvailable:function(){
				if(typeof window.localStorage == 'undefined') return false;
				var test = 'testStorage';
				try {
					localStorage.setItem(test, test);
					localStorage.removeItem(test);
					return true;
				} catch(e) {
					return false;
				}
			},
			get:function(key)
			{
				if(this.IsAvailable())
				{
					return localStorage.getItem(key);
				}
				return null;
			},
			set:function(key,value)
			{
				if(this.IsAvailable())
				{
					return localStorage.setItem(key,value);
				}
			},
			remove:function(key)
			{
				if(this.IsAvailable()) localStorage.removeItem(key);
			}

		}

		this.Cookie = {
		    get: getCookie,
		    set: setCookie,
		    setByDomain: setDomainCookie,
		    delByDomain: delDomainCookie
		}
	}

	var BettypeName = new bettypeName;

	var TeamNameRef=new RefName();
	var	LeagueNameRef=new RefName();
	var	HorseTeamNameRef=new RefName();
	var EventHub = {};
	_.extend(EventHub, Backbone.Events);
	var languageHelper = new LanguageHelper();
	var utility = new util();
	var changeTwoDecimal_f = function(num){
		if(num==null) num=0;
        num = num.toString();
        var former = "";
        var latter = "";
        if(num.indexOf(".") >= 0){
            former = num.split(".")[0];
            latter = num.split(".")[1].substring(0,2);
        } else {
            former = num;
        }
        num = Number(former+"."+latter);
        return num;
	};
	var trimDecimal = function (num) {
	    num = num.toString();
	    var dotIdx = num.indexOf(".");
	    if (dotIdx >= 0) {
	        num = num.substr(0, dotIdx);
	    }

	    return num;
	}

	var CheckIPDomainType = function (checkName) {
	    var result = false;
	    var regexIP = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	    result = regexIP.test(checkName);
	    return result;
	}

	var getDomainName = function (hostName) {
	    if (hostName.lastIndexOf(":") > -1) {
	        hostName = hostName.substring(0, hostName.lastIndexOf(":"));
	    }
	    return hostName.substring(hostName.indexOf(".", hostName.indexOf(".") - 1) + 1);
	}

	var getUrlRoot = function (url) {
	    var tmp = url;
	    var result = url;
	    var sIdx = tmp.indexOf("://");
	    if (sIdx > -1) {
	        tmp = url.substring(sIdx + 3);
	        result = url.substring(0, sIdx + 3);
	    }
	    var eIdx = tmp.indexOf("/");
	    if (eIdx > -1) {
	        result += tmp.substring(0, eIdx + 1);
	    }
	    else {
	        result = url;
	    }
	    return result;
	}

	var getMobileOperatingSystem = function(){
			var userAgent = navigator.userAgent || navigator.vendor || window.opera;
			if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
				return 'iOS';
			} else if (userAgent.match(/Android/i)) {
				return 'Android';
			} else {
				return 'unknown';
			}
	}

	//Input must be number type, and Output is string type.
	var addCommas = function(strValue) {
		var objRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
		while (objRegExp.test(strValue)) {
			if(!isNaN(strValue)){
				strValue = strValue.toString();
			}
			strValue = strValue.replace(objRegExp, '$1,$2');
		}
	  return strValue;
	};

	var removeCommas = function (strValue) {
		let tempVal = strValue;
		if (strValue && typeof strValue != "string") tempVal = tempVal.toString();
		return tempVal ? tempVal.replace(/,/g, '') : tempVal;
	}

	var castAmount  = function(amount) {
  		return addCommas(changeTwoDecimal_f(amount).toFixed(2));
	};

	var ConvertDate=function(utcSecTime,offsetHour)
	{
		var offset = (new Date().getTimezoneOffset()*60)+(offsetHour*3600);
		var d = new Date((utcSecTime+offset)*1000);
		return d;
	}

	var RefHdpName = function (hdp) {
	    var res = "";
	    if (languageHelper.get("Lang").toLowerCase() == "zh-cn" || languageHelper.get("Lang").toLowerCase() == "zh-chs") {
	        var transNumber = ["零", "一", "兩", "三", "四", "五", "六", "七", "八", "九"];
	        var tansQuarter = ["{0}球/{0}球半", "{0}球半", "{0}球半/{1}球", "{0}球"];
	        var transName = ["", "平手/半球", "半球", "半球/一球", "一球", "一球/球半", "球半", "球半/两球", "两球"];
	        var hdpIndex = parseInt(hdp / 0.25);
	        if (0 < hdpIndex) {
	            if (hdpIndex < transName.length) {
	                res = transName[hdpIndex];
	            }
	            else if (hdpIndex <= 36) {
	                var nmIdx = parseInt(hdp);
	                var qIdx = (hdpIndex - 1) % 4;
	                var tmpName = tansQuarter[qIdx];
	                res = tmpName.replace(/\{0\}/g, transNumber[nmIdx]).replace("{1}", transNumber[nmIdx + 1]);
	            }
	        }
	    }

	    return res;
	}

	var GetRandomNumber = function () {
	    return Math.floor(Math.random() * 100000000);
	}

	function padLeft(str,lenght){
		str+="";
		if(str.length >= lenght)
			return str;
		else
			return padLeft("0" +str,lenght);
	}
	
	function padRight(str,lenght,word){
		str+="";
		if(str.length >= lenght)
			return str;
		else
			return padRight(str+word,lenght,word);
	}

	function transTo24Hour(myTime) {
	    var result = "";
	    regex = /(0[0-9]|1[0-2]|[0-9])\:{1}([0-5]{0,1}[0-9]{1})\s(am|pm)/i;
	    if (regex.test(myTime)) {
	        result = myTime.replace(regex, function (s, hour, min, ampm) {
	            if (ampm.toLowerCase() == "pm") {
	                if (hour == 12) {
	                    return hour + ":" + min
	                }
	                else {
	                    return Number(hour) + 12 + ":" + min
	                }
	            }
	            else if (ampm.toLowerCase() == "am") {
	                if (hour == 12) {
	                    return "00:" + min;
	                }
	                else {
	                    return hour + ":" + min;
	                }
	            }
	            else {
	                return s;
	            }
	        });
	    }
	    else {
	        result = myTime;
	    }

	    return result;
	}

	var StringUtil={
		FirstUpper:function (str) {
			return str.substring(0,1).toUpperCase()+ str.substring(1).toLowerCase();
		}
	};

 	function GroupSeperatorInteger(amount) {
		return amount.toString().replace(/^(-?\d+)((\.\d*)?)$/, function (s, s1, s2) { return s1.replace(/\d{1,3}(?=(\d{3})+$)/g, "$&,"); });
	};

	function GroupSeperator(amount) {
		return amount.toString().replace(/^(-?\d+)((\.\d*)?)$/, function (s, s1, s2) { return s1.replace(/\d{1,3}(?=(\d{3})+$)/g, "$&,") + ((s2 == "") ? ".00" : s2); });
	};

	function RemoveSeperator(myString) {
	    var pureDecimal = myString.replace(/\,/g, "");
        return pureDecimal;
	}

	function ordinal_suffix_of(i) {
	    var j = i % 10,
	        k = i % 100;
	    if (j == 1 && k != 11) {
	        return i + "st";
	    }
	    if (j == 2 && k != 12) {
	        return i + "nd";
	    }
	    if (j == 3 && k != 13) {
	        return i + "rd";
	    }
	    return i + "th";
	}

	function convert24Hto12H (date) {
		if(date instanceof Date)
		{
			var hours = date.getHours() ;
			var AmOrPm = hours >= 12 ? 'pm' : 'am';
			hours = (hours % 12) || 12;
			
			var minutes = date.getMinutes() ;
			minutes = ('00' + minutes).substr(('00' + minutes).length-2, 2);

			var finalTime = hours + ":" + minutes + " " + AmOrPm;
			return finalTime;
		} else {
			return "00:00 AM"
		}
	}
	
	function convertDatetoUTC(date) {
		var now_utc =  Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
		date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());

		return new Date(now_utc);
	}
	
	function eEurocupUrl(lang){
		switch(lang)
		{
			case "ch":
			case "cs":
			case "zhcn":
				lang="index.html";
				break;
			case "vn":
				lang="index_vn.html";
				break;
			case "th":
				lang="index_th.html";
				break;
			default:
				lang="index_en.html";
				break;

		}
		return "https://www.sabasports.com/"+lang;	
	}

    
	
	function PopupWindow (wName, gotoUrl) {
		if (gotoUrl) {
			if (getMobileOperatingSystem() == "iOS") {
				if (window._myWindow[wName]) {
					window._myWindow[wName]["objWin"].close();
				}
				window._myWindow[wName] = {};
				window._myWindow[wName]["objWin"] = window.open(gotoUrl, wName);
				if (window._myWindow[wName]["objWin"]) {
					window._myWindow[wName]["navKey"] = gotoUrl;
				}
			}
			else {
				if (window._myWindow[wName] && window._myWindow[wName]["navKey"] == gotoUrl && window._myWindow[wName]["objWin"].closed == false) {
					window._myWindow[wName]["objWin"].focus();
				}
				else {
					window._myWindow[wName] = {};
					window._myWindow[wName]["objWin"] = window.open(gotoUrl, wName);
					if (window._myWindow[wName]["objWin"]) {
						window._myWindow[wName]["navKey"] = gotoUrl;
					}
				}
			}
		}
	}
	
	class GuestureSlide {
		    
			
		// class 的建構子
		constructor(targetDom, callback) {
			
			
           
            this.startX=0;
			this.startY=0;
           
            this.moveLength = 0;  // 手指当前滑动的距离
            this.direction = 1; // 滑动的方向
            this.isMove = false; // 是否发生左右滑动
            this.startT = 0; // 记录手指按下去的时间
            this.isTouchEnd = true; // 标记当前滑动是否结束(手指已离开屏幕) 
		    
			
			
			this.targetDom = targetDom;
			this.callback = callback;
			
			this.onTouchstartPoint =this.onTouchstart.bind(this);
			this.onTouchmovePoint =this.onTouchmove.bind(this);
			this.onTouchendPoint =this.onTouchend.bind(this);
			
			
			this.targetDom.on('touchstart', this.onTouchstartPoint);
			this.targetDom.on('touchmove', this.onTouchmovePoint);
			this.targetDom.on('touchend', this.onTouchendPoint);
			
		}
		
		Dispose()
		{
			this.targetDom.off('touchstart', this.onTouchstartPoint);
			this.targetDom.off('touchmove', this.onTouchmovePoint);
			this.targetDom.off('touchend', this.onTouchendPoint);
		}
		
		// class 的方法
		onTouchstart(e) {
			if (e.originalEvent.touches.length === 1 || isTouchEnd) 
			{
                   var touch = e.originalEvent.touches[0];
                   this.startX = touch.pageX;
                   this.startY = touch.pageY;
                  
                   this.startT = + new Date(); // 记录手指按下的开始时间
                   this.isMove = false; // 是否产生滑动
                   this.isTouchEnd = false; // 当前滑动开始
            }
		}
		
		onTouchmove(e) {
			// e.preventDefault();
               
               // 如果当前滑动已结束，不管其他手指是否在屏幕上都禁止该事件
               if (this.isTouchEnd) return ;
               
               let touch = e.originalEvent.touches[0];
               let deltaX = touch.pageX - this.startX;
               let deltaY = touch.pageY - this.startY;
               
			   if(Math.abs(deltaX) > Math.abs(deltaY))
			   {		   
			   
				   
				   this.isMove = true;
				   this.moveLength = Math.abs(deltaX);
				   this.direction = deltaX > 0 ? -1 : 1; // 判断手指滑动的方向
			   }
		}
		
		onTouchend(e) {
			//e.preventDefault();
               let translate = 0;
               // 计算手指在屏幕上停留的时间
               let deltaT = + new Date() - this.startT;
               // 发生了滑动，并且当前滑动事件未结束
               if (this.isMove && !this.isTouchEnd) { 
                    this.isTouchEnd = true; // 标记当前完整的滑动事件已经结束 
                    // 使用动画过渡让页面滑动到最终的位置
                    //viewport.style.webkitTransition = '0.3s ease -webkit-transform';
                    if (deltaT < 300 && this.moveLength>10) { // 如果停留时间小于300ms,则认为是快速滑动，无论滑动距离是多少，都停留到下一页
                       
						if(this.callback) this.callback(this.direction);
						//console.log(this.direction);
                    } else {
                       
                    }
                   
                   
                }
		}
	}

	function PNRLog(eventid , match) // Only for Event type 18, 19
	{
		SyncServer("Default/PNRLog", {eventid:eventid,match:match});
	}

	function WritePNRLog(custid,matchid,platform,eventtype){
		let _platform = platform ? platform : window._SkinMode == 0 ? 'n' : 'l';

		//custid,matchid,siteid,platform,eventtype
		let parm = `${custid},${matchid},${window._Mesid},${_platform},${eventtype}`;
		SyncServer('Odds/WritePNRLog',{parm:parm},function(val){
			if(val.ok!="")
			{
			  console.log('WritePNRLog success.parm:'+parm);
			}
 		 });
	}
	
	function openCreditRnR() {
		const lang = CultureToRef(utility.Cookie.get("_Mculture") ? utility.Cookie.get("_Mculture") : "en-US");
		const darkMode = utility.Cookie.get("darkmode");
		const url = window.siteSetting.CDNUrl + "/RnR/" + window._Site + "/" + (lang == "zhcn" ? "cs" : lang) + "/RnR.html?layout=mobile&darkmode=" + (darkMode == "true" && window._SkinMode == 3);
		require(["router"], function (router) {
			router.OpenNewWindow(url, "c1");
		});
	}
	
	function ConvertTimeLanguage(ShowTimeVal,onlyheader)
	{
		let ShowTime = ShowTimeVal;
		switch(ShowTime.substr(0,2))
		{
			
			case "1Q":
				ShowTime = languageHelper.Get("lbl_1Q");
				break;
			case "2Q":
				ShowTime = languageHelper.Get("lbl_2Q");
				break;
			case "3Q":
				ShowTime = languageHelper.Get("lbl_3Q");
				break;
			case "4Q":
				ShowTime = languageHelper.Get("lbl_4Q");
				break;
			case "1H":
				ShowTime = languageHelper.Get("lbl_1H");
				break;
			case "2H":
				ShowTime = languageHelper.Get("lbl_2H");
				break;
			case "1P":
				ShowTime = languageHelper.Get("livescoreHeader_1P");
				break;
			case "2P":
				ShowTime = languageHelper.Get("livescoreHeader_2P");
				break;
			case "3P":
				ShowTime = languageHelper.Get("livescoreHeader_3P");
				break;
			case "OT":
				ShowTime = languageHelper.Get("lbl_OT");
				break;
				
		}
		
		if(!onlyheader && ShowTime!=ShowTimeVal)
		{
			ShowTime=ShowTime+ShowTimeVal.substr(2);
		}
		return ShowTime;
	}


	function GetShowTimeCNJson(match, Is12, sysT) {
		let ret = {
			ShowDate: "",
			ShowTime: ""
		}
		var IsLive = match["IsLive"];
		var sportid = match["GameID"];
		if (match["Tsp"]) {
			if (sportid == 1) {
				ret.ShowDate = languageHelper.Get("Ref_IR");
			}
			if (IsLive) {
				switch (sportid) {
					case 50:
						ret.ShowTime = languageHelper.Get('lbl_Crckt_Stumps');
						break;
					case 2:
					case 3:
					case 4:
					case 28:
					case 6:
					case 47:
					case 49:
					case 51:
					case 45:
					case 18:
					case 9:
					case 48:
						ret.ShowTime = languageHelper.Get('lbl_TOUT');
						break;
				}
			}
			return ret;
		}
		else if (IsLive && match["HLS"] == 0 && sportid == 44) {
			ret.ShowDate = languageHelper.Get("Ref_live");
			ret.ShowTime = languageHelper.Get('lbl_round') + match["T1V"];
			return ret;
		}

		if (IsLive) {
			if (sportid==8 || sportid==9 || sportid==43){
				if(sportid==8) {
					ret.ShowDate = "";
				}else if(sportid==9){
					ret.ShowDate = languageHelper.Get("Ref_live");
				}else{
					if(IsLive){
						if (match["ISS"]==0) {
							ret.ShowDate = languageHelper.Get("lbl_Status_InPlay");
						}else{
							ret.ShowDate = languageHelper.Get("Ref_live");
						}
					}
				}
			}
			else {
				ret.ShowDate = languageHelper.Get("Ref_live");
			}
		}
		
		var ShowTime=match["ShowTime"]?match["ShowTime"]:"";
		if(ShowTime!="" && ShowTime.substring(0, 3)=="Ref" ){
			ShowTime=languageHelper.Get(ShowTime);
			if(ShowTime==null) ShowTime="";
			ret.ShowDate = ShowTime.replace('</</','</');
		}
		else if(!!~[2, 55, 56, 99].indexOf(sportid) && match["ltm"]) {
			if(sportid == 56 || sportid == 99) {
				ret.ShowTime = "";
			}
			else {
				var ltm = match["ltm"];
				var sec = 0;
				if (ltm[2] == 0) {
					sec = Math.round((sysT.get("Time").getTime() - new Date(ltm[1]).getTime()) / 1000);
				} else if (ltm[2] < 0) {
					sec = Math.abs(ltm[2]);
				}
				sec = ltm[0] * 60 - sec;
				if (sec < 0) sec = 0;
				var min = Math.floor(sec / 60);
				sec = sec - min * 60;
				if (sportid == 2) {
					ret.ShowTime = ConvertTimeLanguage(ShowTime,true) + " " + padLeft(min, 2) + ":" + padLeft(sec, 2);
				}
				else {
					ret.ShowTime = padLeft(min, 2) + ":" + padLeft(sec, 2);
				}
			}
		}
		else if (IsLive && sportid==43) {
			let mapIdx = ShowTime.replace(/(\d).*/i, function(s, s1) { return s1 });
			ret.ShowTime = (languageHelper.Get("lbl_map" + mapIdx)) ? languageHelper.Get("lbl_map" + mapIdx) : "Map " + mapIdx;
		}
		else {
			ret.ShowTime = ConvertTimeLanguage(ShowTime);
		}

		if (!IsLive)
		{
			var d = new Date(match["GameTime"]+'-04:00');
			var nd=d;
			var Month = (nd.getMonth()+1)<10?"0"+(nd.getMonth()+1):nd.getMonth()+1;
			var day = nd.getDate() <10?"0"+nd.getDate():nd.getDate();
			var Hours = nd.getHours() <10?"0"+nd.getHours():nd.getHours();
			var AMPM="";
			if(Is12)
			{
				if(nd.getHours()>=12) 
				{
					Hours= "" + (nd.getHours()==12?"12":nd.getHours()-12);
					AMPM=" PM";
				}else{
					AMPM=" AM";
					Hours = "" + (nd.getHours()==0?"12":nd.getHours());
				}
				if(Hours.length<2) Hours="0"+Hours;
			}
			var Minutes =nd.getMinutes() <10?"0"+nd.getMinutes():nd.getMinutes();
			ret.ShowDate = Month + "/" + day;
			ret.ShowTime = Hours + ":" + Minutes + AMPM;
		}
		ret.ShowDate = ret.ShowDate.replace('</</','</');
		ret.ShowTime = ret.ShowTime.replace('</</','</');

		return ret;
	}

	const getCurrentVersion = function () {
		if (window._SkinMode == 0) return "Classic"
		if (window._SkinMode == 3 && !window._EuroView) return "Compact";
		if (window._SkinMode == 3 && window._EuroView) return "EuroView";
		return null;
	}

	const getCompareVersion = function(version) {
		return version == getCurrentVersion();
	}

	const getVSStreamingDefine = function () {
		switch (window._VSStreamingSource) {
			case "1":
				return {
					180: {
						en: ["Virtual22", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/30254c27483141328b3c1416c06b053b/manifest.m3u8"],
						ch: ["Virtual23", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/1ace65d7eb5e42c09093844df8357efd/manifest.m3u8"],
						cs: ["Virtual24", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/8f35b9cf56c043ed90cd00f434f6878a/manifest.m3u8"]
					},
					181: {
						en: ["Virtual1", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/3bbfbf20483748de841dd6d0fef13d95/manifest.m3u8"],
						ch: ["Virtual2", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/8c7f49c50afa4315aafe2a62353a334e/manifest.m3u8"],
						cs: ["Virtual3", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/9f6884fc4dc84933975d844cfd63e88a/manifest.m3u8"]
					},
					182: {
						en: ["Virtual4", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/239da94e020a4d5bbc955633ff93324e/manifest.m3u8"],
						ch: ["Virtual5", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/231692e4260e42bb8661ba5854a2585b/manifest.m3u8"],
						cs: ["Virtual6", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/827d1ecb412243ad993299ed0da29d09/manifest.m3u8"]
					},
					183: {
						en: ["Virtual16", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/48c276e38c9948cfb8d44a5466382e38/manifest.m3u8"],
						ch: ["Virtual17", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/16d43d8581b74f219c6daedb9511c343/manifest.m3u8"],
						cs: ["Virtual18", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/f690c8765edb4a02af8b94e48118b390/manifest.m3u8"]
					},
					184: {
						en: ["Virtual7", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/1759dcf276b043859602386052654d8c/manifest.m3u8"],
						ch: ["Virtual8", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/e2a0eccceaaf478fa84687e9099643ec/manifest.m3u8"],
						cs: ["Virtual9", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/a1582558c6a246d3974c04723230dafe/manifest.m3u8"]
					},
					185: {
						en: ["Virtual13", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/dba900cb871f4b2b804fb8965542541e/manifest.m3u8"],
						ch: ["Virtual14", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/2aad4eca945147158bdaabfb7bcabb2e/manifest.m3u8"],
						cs: ["Virtual15", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/b4866a8c43bb4be2ad0deba70fbdb510/manifest.m3u8"]
					},
					186: {
						en: ["Virtual19", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/faf7a55b0f2e4d2bbf19631b09275ad7/manifest.m3u8"],
						ch: ["Virtual20", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/fb3b9fc6d6884ec498cd096b56b08db0/manifest.m3u8"],
						cs: ["Virtual21", "https://sctmmdlive.mmdlive.lldns.net/sctmmdlive/c500b409a5504e7cbca6ab53545e406b/manifest.m3u8"]
					}
				};
			case "2":
			default:
				let url = "https://innohls3.s.llnwi.net/vs/{0}/playlist.m3u8";
				return {
					180: {
						en: ["Virtual22", url.format("Virtual22")],
						ch: ["Virtual23", url.format("Virtual23")],
						cs: ["Virtual24", url.format("Virtual24")]
					},
					181: {
						en: ["Virtual1", url.format("Virtual1")],
						ch: ["Virtual2", url.format("Virtual2")],
						cs: ["Virtual3", url.format("Virtual3")]
					},
					182: {
						en: ["Virtual4", url.format("Virtual4")],
						ch: ["Virtual5", url.format("Virtual5")],
						cs: ["Virtual6", url.format("Virtual6")]
					},
					183: {
						en: ["Virtual16", url.format("Virtual16")],
						ch: ["Virtual17", url.format("Virtual17")],
						cs: ["Virtual18", url.format("Virtual18")]
					},
					184: {
						en: ["Virtual7", url.format("Virtual7")],
						ch: ["Virtual8", url.format("Virtual8")],
						cs: ["Virtual9", url.format("Virtual9")]
					},
					185: {
						en: ["Virtual13", url.format("Virtual13")],
						ch: ["Virtual14", url.format("Virtual14")],
						cs: ["Virtual15", url.format("Virtual15")]
					},
					186: {
						en: ["Virtual19", url.format("Virtual19")],
						ch: ["Virtual20", url.format("Virtual20")],
						cs: ["Virtual21", url.format("Virtual21")]
					}
				};
		}
	}
	
	const GetLiveScore = function(match,sportId,team,isLive,options)
	{
		let score = [];
		if (!!~[55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 99].indexOf(sportId)) return score;

		const lS = match.get("ls");
		const HLS = match.get("HLS");
		switch(sportId)
		{
			case 5:
				if (isLive && HLS == 0) {
					if (lS) {
						let nowSet = lS["llp"] || 1;
						if (team == "home") {
							score.push(match.get("T1V"));
							scoreKey = "h" + nowSet + "s"
							score.push(lS[scoreKey] || "0");
							score.push(lS["hpt"] || "0");
						}
						else {
							score.push(match.get("T2V"));
							scoreKey = "a" + nowSet + "s"
							score.push(lS[scoreKey] || "0");
							score.push(lS["apt"] || "0");
						}
					}
					else {
						score.push("0");
						score.push("0");
						score.push("0");
					}
				}
			break;
			case 6:
			case 9:
			case 18:
				if (isLive && HLS == 0) {
					if (lS) {
						let nowSet = lS["llp"] || 1;
						if (team == "home") {
							score.push(match.get("T1V"));
							scoreKey = "h" + nowSet + "s"
							score.push(lS[scoreKey] || "0");
						}
						else {
							score.push(match.get("T2V"));
							scoreKey = "a" + nowSet + "s"
							score.push(lS[scoreKey] || "0");
						}
					}
					else {
						score.push("0");
						score.push("0");
					}
				}
				break;
			case 44:
				if (team != "away") {
					score.push(languageHelper.Get('lbl_round') + match.get("T1V"));
				}
				break;
			case 50:
				//return Array ex:[{text: string, isPlay: boolean}]
				if (HLS != 1 && lS.bgn != 0 && lS.trn !=0 ) {
					let infoArray = team == "home" ? [[lS.inh1,lS.wh1],[lS.inh2,lS.wh2],[lS.soh,lS.sowh]] 
													: [[lS.ina1,lS.wa1],[lS.ina2,lS.wa2],[lS.soa,lS.sowa]];
					let isPlay = !((team == "home") ^ (lS.trn == "1"));
					let hasPaly = !((team == "home") ^ (lS.bgn == "1"));

					let isPlayInfo = `/${infoArray[lS.llp -1][1]}`;
					
					if (!(options && options.noShowBall)) {
						if (lS.hls == 0) isPlayInfo += ` (Ov${lS.over}.${lS.ball}*)`;
						if (lS.hls == 3) isPlayInfo += ` (Ov${lS.over})`;
					}

					if ((lS.llp == 1 && (isPlay || hasPaly)) || (lS.llp == 2 && !(isPlay || hasPaly))) {
						let myScore = (isPlay) ? infoArray[0][0] + isPlayInfo : infoArray[0][0];
						score.push({text:myScore, isPlay:isPlay});
					} 
					else if (lS.llp == 2) {
						score.push({text:infoArray[0][0]+",",isPlay:false});
						let myScore = (isPlay) ? infoArray[1][0] + isPlayInfo : infoArray[1][0];
						score.push({text:myScore, isPlay:isPlay});
					} 
					else if (lS.llp == 3 && (isPlay || hasPaly)) {
						let myScore = (isPlay) ? infoArray[2][0] + isPlayInfo : infoArray[2][0];
						score.push({text:myScore, isPlay:isPlay});
					}
				}
				break;
			default:
				if(isLive && HLS == 0)
				{
					let score1 = match.get("T1V");
					let score2 = match.get("T2V");
					if (team) {
						score.push((team == "home") ? score1 : score2);
					} else {
						score.push(score1 + " - " + score2);
					}
				}
				break;
		}
		return score;
	}

	const parseColorHslToHex = function(str) { 
		const strArray = str.match(/[0-9]+/g);
		let h = strArray[0];
		let s = strArray[1];
		let l = strArray[2]

		l /= 100;
		const a = s * Math.min(l, 1 - l) / 100;
		const f = function(n){
		  const k = (n + h / 30) % 12;
		  const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
		  return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
		};
		return `#${f(0)}${f(8)}${f(4)}`;
	}

	const createGuid = function () {
		const S4 = function () {
			return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		}
		return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
	}

	return {
		SyncServer:SyncServer,
		SyncServerHtml:SyncServerHtml,
		TeamNameRef:TeamNameRef,
		LeagueNameRef: LeagueNameRef,
		HorseTeamNameRef:HorseTeamNameRef,
		LanguageHelper:languageHelper,
		BettypeName:BettypeName,
		EventHub:EventHub,
		Utility: utility,
		CultureToRef:CultureToRef,
		CasinoLangPath: CasinoLangPath,
		SaveGesture: SaveGesture,
		changeTwoDecimal_f: changeTwoDecimal_f,
		trimDecimal: trimDecimal,
		getDomainName: getDomainName,
		getUrlRoot: getUrlRoot,
		getMobileOperatingSystem:getMobileOperatingSystem,
		addCommas: addCommas,
		removeCommas: removeCommas,
		castAmount: castAmount,
		ConvertDate: ConvertDate,
		RefHdpName: RefHdpName,
		RandomNumber: GetRandomNumber,
		padLeft: padLeft,
		transTo24Hour: transTo24Hour,
		GroupSeperatorInteger: GroupSeperatorInteger,
		GroupSeperator: GroupSeperator,
		RemoveSeperator: RemoveSeperator,
		ordinal_suffix_of : ordinal_suffix_of,
		StringUtil:StringUtil,
		convert24Hto12H: convert24Hto12H,
		convertDatetoUTC: convertDatetoUTC,
		padRight:padRight,
		RefName:RefName,
		GuestureSlide:GuestureSlide,
		PNRLog:PNRLog,
		WritePNRLog:WritePNRLog,
		PopupWindow:PopupWindow,
		eEurocupUrl: eEurocupUrl,
		openCreditRnR: openCreditRnR,
		GetShowTimeCNJson: GetShowTimeCNJson,
		getCompareVersion: getCompareVersion,
		getVSStreamingDefine: getVSStreamingDefine,
		GetLiveScore : GetLiveScore,
		parseColorHslToHex: parseColorHslToHex,
		createGuid: createGuid
	};
});
