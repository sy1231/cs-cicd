function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _router = require('router');

var _router2 = _interopRequireDefault(_router);

function HiWorldHook() {
    return React.createElement(
        "div",
        null,
        "HI World",
        React.createElement(
            "button",
            { onClick: function () {
                    return _router2["default"].navigateAndSaveLast("#HelloWorld", { trigger: true });
                } },
            "hello"
        ),
        React.createElement(
            "button",
            { onClick: function () {
                    return window.location.href = "Default";
                } },
            "default"
        )
    );
}

// class HiWorldComponent1 extends React.Component {
//     render() {
//         return (
//             <div>
//                 HI World！！！
//                 <button onClick={()=>router.navigateAndSaveLast(`#HelloWorld`, { trigger: true })}>hello</button>
//                 <button onClick={()=> window.location.href = "Default"}>default</button>
//             </div>           
//         );
//     }
// }

define([], function () {
    return {
        Main: HiWorldHook
    };
});
//# sourceMappingURL=HiWorld.js.map
