var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//import React from "react";

var _router = require("router");

var _router2 = _interopRequireDefault(_router);

// import { Main as BaseComponent } from 'Commponent/Base';

var _common = require('common');

var _common2 = _interopRequireDefault(_common);

var BASE_URL = "/api";
var USER_URL = BASE_URL + "/user";
var IMG_URL = "/static";
function SelectList(props) {
  return React.createElement(
    "select",
    { name: props.name, value: props.value, onChange: props.onChange },
    React.createElement(
      "option",
      { value: "" },
      "請選擇"
    ),
    props.list && props.list.map(function (item) {
      return React.createElement(
        "option",
        { value: item.value },
        item.name
      );
    })
  );
}

define(['Commponent/Base'], function (BaseComponent) {
  var UserSingle = (function (_BaseComponent) {
    _inherits(UserSingle, _BaseComponent);

    function UserSingle(props) {
      _classCallCheck(this, UserSingle);

      _get(Object.getPrototypeOf(UserSingle.prototype), "constructor", this).call(this, props);
      this.state = {
        row: {
          // id: null,
          name: null,
          hight: null,
          dept: 0,
          projs: null,
          photo: null,
          birthday: null
        },
        photo: null,
        photoFile: null
      };
      this.selectProjAll = this.selectProjAll.bind(this);
      this.changeProj = this.changeProj.bind(this);
      this.getFile = this.getFile.bind(this);
      this.read = this.read.bind(this);
      if (this.props.id > 0) {
        this.read(this.props.id);
      }
    }

    // componentDidMount() {
    //   if (this.props.id > 0) {
    //     this.read(this.props.id);
    //   }
    // }

    _createClass(UserSingle, [{
      key: "read",
      value: function read(id) {
        // UserService.getSingle(id).then((m) => {
        //   this.setState((state) => ({
        //     row: m.data,
        //     photo: m.data.photo,
        //   }));
        // });
        _common2["default"].SyncServer(USER_URL + "/" + id, null, (function (val) {
          this.setState(function (state) {
            return {
              row: val,
              photo: val.photo
            };
          });
        }).bind(this), null, null, "GET");
      }
    }, {
      key: "selectProjAll",
      value: function selectProjAll(e) {
        var checked = e.target.checked;
        var projs = [];
        if (checked) {
          projs = this.props.projs.map(function (m) {
            return m.value;
          });
        }
        var row = this.state.row;
        row.projs = projs;
        this.setState(function (state) {
          return {
            row: row
          };
        });
      }
    }, {
      key: "changeProj",
      value: function changeProj(e) {
        var projs = [];
        if (this.state.row.projs) {
          projs = [].concat(_toConsumableArray(this.state.row.projs));
        }

        var v = parseInt(e.target.value);
        var index = projs.indexOf(v);
        if (e.target.checked) {
          if (index < 0) {
            projs.push(v);
          }
        } else {
          if (index > -1) {
            projs.splice(index, 1);
          }
        }

        var row = this.state.row;
        row.projs = projs;
        this.setState(function (state) {
          return {
            row: row
          };
        });
      }
    }, {
      key: "save",
      value: function save() {
        var row = this.state.row;
        row.id = parseInt(this.props.id);

        if (this.state.photoFile) {
          // await UserService.upload(this.state.photoFile).then((m) => {
          //   row.photo = m.data;
          // });
          var fData = new FormData();
          if (data != null) {
            fData.append("file", this.state.photoFile);
          }
          $.ajax({
            url: USER_URL + "/ufile",
            data: fData,
            method: "POST",
            headers: {
              "Content-Type": "multipart/form-data"
            },
            success: function success(result) {
              row.photo = result;
            }
          });
        }

        if (row.id > 0) {
          //await UserService.put(row);
          //common.SyncServer(`${USER_URL}/${row.id}`, row, null, null, null, "PUT",null,null, "application/json");
          $.ajax({
            url: USER_URL + "/" + row.id,
            data: row,
            method: "PUT",
            headers: {
              "Content-Type": "application/json"
            },
            success: function success(result) {
              row.photo = result;
            }
          });
        } else {
          // await UserService.post(row);
          _common2["default"].SyncServer("" + USER_URL, row, null, null, null);
        }
        this.props.onBack();
      }
    }, {
      key: "getFile",
      value: function getFile(e) {
        //let files:FileList = e.target.value;
        var files = e.target.files;
        this.setState(function (state) {
          return {
            photoFile: files[0]
          };
        });
      }
    }, {
      key: "render",
      value: function render() {
        var _this = this;

        return React.createElement(
          "div",
          null,
          React.createElement(
            "table",
            null,
            React.createElement(
              "tbody",
              null,
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "id"
                ),
                React.createElement(
                  "td",
                  null,
                  this.props.id > 0 && this.props.id
                )
              ),
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "name"
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement("input", {
                    name: "row.name",
                    type: "text",
                    value: this.state.row.name,
                    onChange: this.handleInputChange
                  })
                )
              ),
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "hight"
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement("input", {
                    name: "row.hight",
                    type: "number",
                    value: this.state.row.hight,
                    onChange: this.handleInputChange
                  })
                )
              ),
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "birthday"
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement("input", {
                    name: "row.birthday",
                    type: "date",
                    value: this.state.row.birthday,
                    onChange: this.handleInputChange
                  })
                )
              ),
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "dept"
                ),
                React.createElement(
                  "td",
                  null,
                  React.createElement(SelectList, {
                    name: "row.dept",
                    list: this.props.depts,
                    value: this.state.row.dept,
                    onChange: this.handleSelectNumberChange
                  })
                )
              ),
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "proj",
                  React.createElement("input", { type: "checkbox", onChange: this.selectProjAll })
                ),
                React.createElement(
                  "td",
                  null,
                  this.props.projs && this.props.projs.map(function (item) {
                    return React.createElement(
                      "span",
                      null,
                      React.createElement(
                        "label",
                        null,
                        React.createElement("input", {
                          type: "checkbox",
                          onChange: _this.changeProj,
                          checked: _this.state.row.projs && -1 !== _this.state.row.projs.indexOf(item.value),
                          value: item.value
                        }),
                        item.name
                      )
                    );
                  })
                )
              ),
              React.createElement(
                "tr",
                null,
                React.createElement(
                  "th",
                  null,
                  "photo"
                ),
                React.createElement(
                  "td",
                  null,
                  this.state.photo && React.createElement("img", {
                    height: "200",
                    src: IMG_URL + "/img/" + this.state.photo
                  }),
                  React.createElement("input", { type: "file", onChange: this.getFile })
                )
              )
            )
          ),
          React.createElement(
            "button",
            { onClick: function () {
                return _this.save();
              } },
            "save"
          ),
          React.createElement(
            "button",
            { onClick: function () {
                return _this.props.onBack();
              } },
            "back"
          )
        );
      }
    }]);

    return UserSingle;
  })(BaseComponent);

  var UserSingleDisplay = (function (_React$Component) {
    _inherits(UserSingleDisplay, _React$Component);

    function UserSingleDisplay(props) {
      _classCallCheck(this, UserSingleDisplay);

      _get(Object.getPrototypeOf(UserSingleDisplay.prototype), "constructor", this).call(this, props);
      this.state = { depts: null, projs: null };
      this.onBack = this.onBack.bind(this);
      // (async () => {
      //   let depts = await DeptService.get();
      //   this.setState({
      //     depts: depts.data.map((m) => {
      //       let item = {
      //         value: m.id,
      //         name: m.name,
      //       };
      //       return item;
      //     }),
      //   });

      //   let projs = await ProjService.get();
      //   this.setState({
      //     projs: projs.data.map((m) => {
      //       let item = {
      //         value: m.id,
      //         name: m.name,
      //         checked: false,
      //       };
      //       return item;
      //     }),
      //   });
      // })();
    }

    _createClass(UserSingleDisplay, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        _common2["default"].SyncServer(BASE_URL + "/dept", null, (function (val) {
          this.setState({
            depts: val.map(function (m) {
              var item = {
                value: m.id,
                name: m.name
              };
              return item;
            })
          });
        }).bind(this), false, null, "GET");

        _common2["default"].SyncServer(BASE_URL + "/proj", null, (function (val) {
          this.setState({
            projs: val.map(function (m) {
              var item = {
                value: m.id,
                name: m.name
              };
              return item;
            })
          });
        }).bind(this), false, null, "GET");
      }
    }, {
      key: "onBack",
      value: function onBack() {
        _router2["default"].navigateAndSaveLast("#User", { trigger: true });

        // this.props.history.goBack();
        //return <Redirect to={"/" + rootPath} />;
        // return <Redirect to="/user" />;
      }
    }, {
      key: "render",
      value: function render() {
        var id = this.props.id;
        return React.createElement(UserSingle, {
          id: id,
          depts: this.state.depts,
          projs: this.state.projs,
          onBack: this.onBack
        });
      }
    }]);

    return UserSingleDisplay;
  })(React.Component);

  return UserSingleDisplay;
});
//# sourceMappingURL=UserSingle.js.map
