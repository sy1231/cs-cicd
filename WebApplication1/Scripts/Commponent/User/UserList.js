var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import React from "react";
// import { Main as BaseComponent } from 'Commponent/Base';
//import BaseComponent from 'Commponent/Base'; //TODO 可能要用define

var _router = require('router');

var _router2 = _interopRequireDefault(_router);

var _common = require('common');

var _common2 = _interopRequireDefault(_common);

//TODO 只有第一次有用???
// const BASE_URL = `/api/user`;
// const IMG_URL = `/static`;

define(['Commponent/Base'], function (BaseComponent) {

  var BASE_URL = '/api/user';
  var IMG_URL = '/static';

  var UserList = (function (_BaseComponent) {
    _inherits(UserList, _BaseComponent);

    function UserList(props) {
      _classCallCheck(this, UserList);

      _get(Object.getPrototypeOf(UserList.prototype), 'constructor', this).call(this, props);
      this.state = { list: [], keyWord: "" };
      this.getList = this.getList.bind(this);
      this.queryList = this.queryList.bind(this);
      // this.delete = this.delete.bind(this);
      // this.handleInputChange = this.handleInputChange.bind(this);
      // this.getList();
      console.log('it is UserList');
    }

    _createClass(UserList, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this.getList();
      }
    }, {
      key: 'getList',
      value: function getList() {
        // let res = await UserService.get();
        // this.setState((state) => ({
        //   list: res.data,
        // }));
        _common2['default'].SyncServer('' + BASE_URL, null, (function (val) {
          this.setState(function (state) {
            return {
              list: val
            };
          });
          console.log(val);
        }).bind(this), null, null, "GET");
      }
    }, {
      key: 'queryList',
      value: function queryList() {
        // let res = await UserService.getQuery(this.state.keyWord);
        // this.setState((state) => ({
        //   list: res.data,
        // }));
        _common2['default'].SyncServer(BASE_URL + '/query', this.state.keyWord, (function (val) {
          this.setState(function (state) {
            return {
              list: val
            };
          });
        }).bind(this));
      }
    }, {
      key: 'delete',
      value: function _delete(id) {
        // let res = await UserService.delete(id);
        _common2['default'].SyncServer(BASE_URL + '/' + id, null, (function () {
          if (this.state.keyWord != "") {
            this.queryList();
          } else {
            this.queryList();
          }
        }).bind(this), null, null, "DELETE");
      }
    }, {
      key: 'create',
      value: function create() {
        this.props.history.push(BASE_URL + '/0');
      }
    }, {
      key: 'render',
      value: function render() {
        var _this = this;

        return React.createElement(
          'div',
          null,
          React.createElement('input', {
            name: 'keyWord',
            type: 'text',
            value: this.state.keyWord,
            onChange: this.handleInputChange
          }),
          React.createElement(
            'button',
            { onClick: this.queryList },
            'query'
          ),
          React.createElement('br', null),
          React.createElement(
            'button',
            { onClick: function () {
                return _this.create();
              } },
            'create'
          ),
          React.createElement(
            'table',
            { className: 'table' },
            React.createElement(
              'thead',
              null,
              React.createElement(
                'tr',
                null,
                React.createElement(
                  'th',
                  null,
                  'id'
                ),
                React.createElement(
                  'th',
                  null,
                  'name'
                ),
                React.createElement(
                  'th',
                  null,
                  'hight'
                ),
                React.createElement(
                  'th',
                  null,
                  'birthday'
                ),
                React.createElement(
                  'th',
                  null,
                  'photo'
                ),
                React.createElement(
                  'th',
                  null,
                  'edit'
                ),
                React.createElement(
                  'th',
                  null,
                  'del'
                )
              )
            ),
            React.createElement(
              'tbody',
              null,
              this.state.list.map(function (item) {
                return React.createElement(
                  'tr',
                  null,
                  React.createElement(
                    'td',
                    null,
                    item.id
                  ),
                  React.createElement(
                    'td',
                    null,
                    item.name
                  ),
                  React.createElement(
                    'td',
                    null,
                    item.hight
                  ),
                  React.createElement(
                    'td',
                    null,
                    item.birthday
                  ),
                  React.createElement(
                    'td',
                    null,
                    item.photo && React.createElement('img', { height: '50', src: IMG_URL + '/img/' + item.photo })
                  ),
                  React.createElement(
                    'td',
                    null,
                    React.createElement(
                      'button',
                      { onClick: function () {
                          return _router2['default'].navigateAndSaveLast('#User/id=' + item.id, { trigger: true });
                        } },
                      'read'
                    )
                  ),
                  React.createElement(
                    'td',
                    null,
                    React.createElement(
                      'button',
                      { onClick: function () {
                          return _this['delete'](item.id);
                        } },
                      'del'
                    )
                  )
                );
              })
            )
          )
        );
      }
    }]);

    return UserList;
  })(BaseComponent);

  return UserList;
});
/* <Link to={`/${rootPath}/${item.id}`}>read</Link> */
//# sourceMappingURL=UserList.js.map
