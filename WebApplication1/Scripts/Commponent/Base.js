var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; desc = parent = undefined; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var BaseComponent = (function (_React$Component) {
  _inherits(BaseComponent, _React$Component);

  function BaseComponent(props) {
    _classCallCheck(this, BaseComponent);

    _get(Object.getPrototypeOf(BaseComponent.prototype), "constructor", this).call(this, props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSelectNumberChange = this.handleSelectNumberChange.bind(this);
  }

  _createClass(BaseComponent, [{
    key: "handleInputChange",
    value: function handleInputChange(event) {
      var target = event.target;
      var name = target.name;

      var state = this.state;
      var names = name.split(".");
      if (names.length > 1) {
        state = this.state[names[0]];
        name = names[1];
      }

      var value = undefined;
      switch (target.type) {
        case "checkbox":
          value = target.checked;
          break;
        case "select-multiple":
          // let flavors = this.state.flavors;
          var list = state[name]; //this.state.row[name];
          var index = list.indexOf(target.value);
          if (index > -1) {
            list.splice(index, 1);
          } else {
            list.push(target.value);
          }
          value = list;
          break;
        case "number":
          value = parseInt(target.value);
          break;
        default:
          value = target.value;
          break;
      }
      console.log(value);

      state[name] = value;
      this.setState(state);
      // let row = this.state.row;
      // row[name] = value;
      // this.setState({
      //   row: row,
      // });
    }
  }, {
    key: "handleSelectNumberChange",
    value: function handleSelectNumberChange(event) {
      var target = event.target;
      var name = target.name;
      var state = this.state;
      var names = name.split(".");
      if (names.length > 1) {
        state = this.state[names[0]];
        name = names[1];
      }

      var value = undefined;
      value = parseInt(target.value);
      state[name] = value;
      this.setState(state);
    }
  }]);

  return BaseComponent;
})(_react2["default"].Component);

var handleInputChange = function handleInputChange(event, origin, setState) {
  var target = event.target;
  var name = target.name;
  var state = origin;
  var names = name.split(".");
  if (names.length > 1) {
    state = state[names[0]];
    name = names[1];
  }
  var value = 1;

  state[name] = value;

  setState(_extends({}, origin, { state: state }));
};

define([], function () {
  return BaseComponent;
  // return  {
  //     Main: BaseComponent,
  //     handleInputChange: handleInputChange,
  // }  
});
//# sourceMappingURL=Base.js.map
