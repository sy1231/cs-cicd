define(['react', 'router', 'react.backbone'], function (React, router) {
    var Main = React.createBackboneClass({
        render: function render() {
            return React.createElement(
                'div',
                null,
                'HI Define',
                React.createElement(
                    'button',
                    { onClick: function () {
                            return router.navigateAndSaveLast('#HelloDefine', { trigger: true });
                        } },
                    'hello'
                ),
                React.createElement(
                    'button',
                    { onClick: function () {
                            return window.location.href = "Default";
                        } },
                    'default'
                )
            );
        }
    });

    return { Main: Main };
});
//# sourceMappingURL=HiWorld.js.map
