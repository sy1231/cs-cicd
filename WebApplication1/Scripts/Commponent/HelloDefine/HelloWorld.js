define(['react', 'router', 'react.backbone'], function (React, router) {
    var Main = React.createBackboneClass({
        onClickItem: function onClickItem() {
            router.navigateAndSaveLast('#HiDefine', { trigger: true });
        },
        render: function render() {
            var _this = this;

            return React.createElement(
                'div',
                null,
                'Hello Define',
                React.createElement(
                    'button',
                    { onClick: function () {
                            return _this.onClickItem();
                        } },
                    'hi'
                ),
                React.createElement(
                    'button',
                    { onClick: function () {
                            return window.location.href = "/#logout";
                        } },
                    'logout'
                )
            );
        }
    });

    return { Main: Main };
});
//# sourceMappingURL=HelloWorld.js.map
