﻿require.config({
	baseUrl: "/Scripts/",
	urlArgs:"v=" + (new Date()).getTime(),
	waitSeconds: 2000,	
	paths: {
		"global/window": "video/document",
		"global/document": "video/document",
		"underscore": "underscore-min",
		"jquery": "jquery-3.3.1.min",
		"backbone": "backbone-min",
		"react": "react/react.production.min",
		"create-react-class": "react/create-react-class",
		"react-dom": "react/react-dom.production.min",
		"react.backbone": "react.backbone",
		"touchswipe.min": "jquery.touchSwipe.min",
		"backbone-redux": "react/backbone-redux",
		"redux": "react/redux.min",
		"react-redux": "react/react-redux.min",
		"seal": "seal/bundle",
		"trailblazerlib": window.location.protocol + "//Gvweb.garcade.net:" + (window.location.protocol == "https:" ? "8097" : "8096") + "/js/trailblazerlib-mobile",
	
		//-- New Bundle Mode Start --//		
		//-- New Bundle Mode End --//
	},
	bundles: {
		'trailblazerlib': ['io', 'trailblazer']
	},
	shim: {
		"jquery": {
			exports: "$"
		},
		"signalr.core": {
			deps: ["jquery"],
			exports: "$.connection"
		},
		"signalr.hubs": {
			deps: ["signalr.core"],
		}
	},
	map: {
		'*': {
			'css': 'css.min'

		}
	}
});