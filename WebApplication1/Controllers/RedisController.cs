﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.App_Code;
using System.Runtime.Serialization.Formatters.Binary;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/redis")]
    public class RedisController : ApiController
    {
        public string Get()
        {
            //long result = 0;
            //long.TryParse(RedisUtil.GetStrings().Get("id"), out result);
            //return result.ToString();

            //return RedisUtil.GetStrings().Get("id");
            return RedisUtil.GetStringsMy().Get("id");
            //return RedisUtil.GetDatabase().StringGet("id");            
        }

        // GET api/values/5
        public string Get(int id)
        {
            //RedisUtil.GetStrings().Set("id", id.ToString());
            RedisUtil.GetStringsMy().Set("id", id.ToString());
            //RedisUtil.GetDatabase().StringSet("id", id.ToString());
            return "finish";
        }

        [HttpGet]
        [Route("t")]
        public TestModel GetT()
        {
            TestModel tmp = RedisUtil.GetRedis("127.0.0.1:6379").Get<TestModel>("id_T");
            return tmp;
        }        
    }
}
