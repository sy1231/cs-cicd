using System.Collections.Generic;
using System.Web.Http;
using WebApplication1.Services;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProjController : ApiController
    {
        private readonly ProjService _service;
        public ProjController(
            ProjService service)
        {
            _service = service;
        }  
        
        [HttpGet]
        public IEnumerable<Proj> Get()
        {
            return _service.GetAll();
        }

        [HttpGet]
        public IHttpActionResult GetSingle(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var data = _service.GetSingle(id);
        
            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        } 
    }
}