using System.Web.Http;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/init")]
    public class InitController: ApiController
    {
        private readonly DeptService _deptService;
        private readonly ProjService _projService;
        private readonly UserService _userService;

        private readonly InitService _initService;

        public InitController(
            DeptService deptService,
            ProjService projService,
            UserService userService,
            InitService initService
            )
        {
            _deptService = deptService;
            _projService = projService;
            _userService = userService;
            _initService = initService;
        }  

        [HttpGet]
        [Route("dept")]
        public IHttpActionResult InitDept()
        {
            _deptService.Init();
            return Ok();
        }

        [HttpDelete]
        [Route("dept")]
        public IHttpActionResult ClearDept()
        {
            _deptService.Clear();
            return Ok();
        }     

        [HttpGet]
        [Route("proj")]
        public IHttpActionResult InitProj()
        {
            _projService.Init();
            return Ok();
        }

        [HttpDelete]
        [Route("proj")]
        public IHttpActionResult ClearProj()
        {
            _projService.Clear();
            return Ok();
        }   
        
        [HttpGet]
        [Route("user")]
        public IHttpActionResult InitUser()
        {
            _userService.Init();
            return Ok();
        }

        [HttpDelete]
        [Route("user")]
        public IHttpActionResult ClearUser()
        {
            _userService.Clear();
            return Ok();
        }

        [HttpGet]
        [Route("all")]
        public IHttpActionResult InitAll()
        {
            _initService.Init();
            return Ok();
        }

        [HttpDelete]
        [Route("all")]
        public IHttpActionResult ClearAll()
        {
            _initService.Clear();
            return Ok();
        }                
    }
}