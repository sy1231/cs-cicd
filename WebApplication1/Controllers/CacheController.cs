﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Commons;

namespace WebApplication1.Controllers
{
    public class CacheController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            Caching.Set("time", System.DateTime.Now.ToString());
            var result = Caching.Get("time");

            return Ok(result);
        }
    }
}
