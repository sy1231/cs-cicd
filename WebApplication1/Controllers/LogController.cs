﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplication1.App_Code;
using System.Runtime.Caching;

namespace WebApplication1.Controllers
{
    [RoutePrefix("api/log")]
    public class LogController : ApiController
    {        
        private static NLog.Logger _BettingFailLogger = NLog.LogManager.GetLogger("BettingFail");
        private static NLog.Logger _Bonus = NLog.LogManager.GetLogger("Bonus*");
        private static NLog.Logger _LoyaltyNlog = NLog.LogManager.GetLogger("Loyalty");
        private static NLog.Logger _Nlogger = NLog.LogManager.GetCurrentClassLogger();
        private static NLog.Logger _UserInfo = NLog.LogManager.GetLogger("UserInfo");
        private static NLog.Logger _3RD = NLog.LogManager.GetLogger("3rd");

        [HttpGet]       
        public IHttpActionResult Get()
        {
            SessionTemp.SiteName = "NLOG";

            _BettingFailLogger.Log(NLog.LogLevel.Info, " _BettingFailLogger.Log(NLog.LogLevel.Info, ...)");
            _Bonus.Debug(" _Bonus.Debug(NLog.LogLevel.Info, ...)");
            _LoyaltyNlog.Info(" _LoyaltyNlog.Info(...)");
            _UserInfo.Info("_UserInfo.Info(...)");
            _Nlogger.Debug("_Nlogger.Debug(...)");
            _Nlogger.Error("_Nlogger.Error(...)");
            _3RD.Info("_3RD.Info(...)");
            _3RD.Debug("_3RD.Debug(...)");
            _3RD.Error("_3RD.Error(...)");
            _3RD.Trace("_3RD.Trace(...)");
            _3RD.Log(NLog.LogLevel.Info, "_3RD.Log(...)");

            try
            {
                throw new Exception($"test error-3rd");
            }
            catch (Exception ex)
            {
                _3RD.Error(ex, " _3RD.Error(ex, '...')");
                _3RD.Error(ex);
            }

            try {
                throw new Exception($"test error-1");
            }
            catch(Exception ex){
                _LoyaltyNlog.Error(ex, "LoyaltyNlog.Error(ex, '...')");
            }
            try
            {
                throw new Exception($"test error-2");
            }
            catch (Exception ex)
            {
                _LoyaltyNlog.Error(ex);             
            }
            try
            {
                throw new Exception($"test error-3");
            }
            catch (Exception ex)
            {
                _Nlogger.Error(ex, "_Nlogger.Error(ex, ...)");
            }
            try
            {
                throw new Exception($"test error-4");
            }
            catch (Exception ex)
            {
                _Bonus.Error(ex, " _Bonus.Error(ex, '...')");
            }
            //try
            //{
            //    throw new Exception($"test error-2");
            //}
            //catch (Exception ex)
            //{
            //    _LoyaltyNlog.Error(ex, "LoyaltyNlog.Error(ex, '...')");
            //    _LoyaltyNlog.Error(ex);
            //    _Nlogger.Error(ex, "_Nlogger.Error(ex, ...)");
            //}

            var res = "ok log";
            return Ok($"{res}");
        }

        [HttpGet]
        [Route("ibc")]
        public IHttpActionResult GetIbc()
        {
            SessionTemp.SiteName = "IBC";

            IBCFile.Logger.Log("Log-FileName1", "Log-LogStr1");
            IBCFile.Logger.Log("Log(...)");

            IBCFile.Logger.Log("3rd", "Log(3rd, ...);");//會產生-3rd-檔案
            IBCFile.Logger.Log("3rd2", "Log(3rd2, ...);"); //3rd2的設定不存在-會到預設的檔案

            IBCFile.Logger.Log2File("Log2File(...)");

            IBCFile.Logger.Log("Log-FileName2", IBCFile.WriteLevel.Error, " Log(Log-FileName2, IBCFile.WriteLevel.Error, ....)");

            try
            {
                throw new Exception("test error-1");
            }
            catch (Exception ex)
            {
                IBCFile.Logger.LogException(ex, "LogException(ex, ...)");
            }
            try
            {
                throw new Exception("test error-2");
            }
            catch (Exception ex)
            {
                IBCFile.Logger.LogException(ex);             
            }
            try
            {
                throw new Exception("test error-3");
            }
            catch (Exception ex)
            {
                IBCFile.Logger.GetExceptionLog(ex, "GetExceptionLog(ex, ...)");
            }
            //try
            //{
            //    throw new Exception("test error!");
            //}
            //catch (Exception ex)
            //{
            //    IBCFile.Logger.LogException(ex, "IBCFile.Logger.LogException(ex, ...)");
            //    IBCFile.Logger.LogException(ex);
            //    IBCFile.Logger.GetExceptionLog(ex, "IBCFile.Logger.GetExceptionLog(ex, ...)");
            //}
           
            return Ok("ok");
        }
    }
}
