using System.Collections.Generic;
using WebApplication1.Models;
using System.IO;
using WebApplication1.Repositories;
using WebApplication1.Services;
using System.Web.Http;
using System.Web;

namespace WebApplication1.Controllers
{
    public class UserController : ApiController
    {
        private readonly UserService _service;
        public UserController(UserService service)
        {
            _service = service;
        }

        // GET: api/User
        // [EnableCors]
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return _service.GetAll();
        }

        // GET: api/User/5
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var data = _service.GetSingle(id);

            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }

        // PUT: api/User/5
        [HttpPut]
        public IHttpActionResult Put(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            user.Id = id;
            // if (id != user.Id)
            // {
            //     return BadRequest();
            // }

            // _context.Entry(user).State = EntityState.Modified;

            _service.Update(user);

            return Ok();
        }

        // POST: api/User
        [HttpPost]
        public IHttpActionResult Post([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Create(user);
            return Ok(user);
        }

        // DELETE: api/User/5]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Delete(id);

            return Ok();
        }

        // private bool Exists(int id)
        // {
        //     return _repository.DbSet().Any(e => e.Id == id);
        // }

        [HttpPost]
        [Route("ufile")]
        public string Upload(HttpPostedFileBase file)
        {
            if (file != null)
            {
                return FileService.Upload(file, "img");
            }
            return "";
        }

        [HttpPost]
        [Route("query")]
        public IEnumerable<User> Query([FromBody] string name)
        {
            return _service.Query(name);
        }
    }
}