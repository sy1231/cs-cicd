﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1.Controllers
{
    public class DiControllerFactory : DefaultControllerFactory
    {
        private readonly Container _container;

        public DiControllerFactory(Container container)
        {
            _container = container;
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (ReferenceEquals(controllerType, null))
            {
                throw new HttpException(404, "Controller not found!");
            }

            return _container.GetInstance(controllerType) as IController;

        }


    }
}