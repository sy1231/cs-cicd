﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{    
    public class LoginController : Controller
    {
        public ActionResult Index(string username, string Password)
        {
            var data = new
            {
                username = username,
                password = Password
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }        
    }
}