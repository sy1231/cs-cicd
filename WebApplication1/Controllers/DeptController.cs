using System.Collections.Generic;
using System.Web.Http;
using WebApplication1.Services;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DeptController : ApiController
    {
        private readonly DeptService _service;
        public DeptController(
            DeptService service)
        {
            _service = service;
        }

        [HttpGet]
        public IEnumerable<Dept> Get()
        {
            return _service.GetAll();
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var data = _service.GetSingle(id);

            if (data == null)
            {
                return NotFound();
            }
            return Ok(data);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.Delete(id);

            return Ok();
        }
    }
}