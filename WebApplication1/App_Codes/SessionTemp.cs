﻿using System;
using System.Web;

namespace WebApplication1.App_Code
{

    public class SessionTemp
    {
        public static String SiteName
        {
            get
            {
                return HttpContext.Current.Session["SiteName"] == null ? "" : HttpContext.Current.Session["SiteName"].ToString();
            }

            set
            {
                HttpContext.Current.Session["SiteName"] = value;               
            }
        }
    }
}