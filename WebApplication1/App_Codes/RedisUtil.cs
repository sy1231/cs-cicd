﻿using ONEbook.Common.Caching.RedisLib;
using RedisLib;
using StackExchange.Redis;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication1.App_Code
{

    public class RedisStringsNew : IRedisStrings
    {
        private IDatabase database = null;
        public RedisStringsNew(string hostname)
        {
            var options = ConfigurationOptions.Parse(hostname);
            var connection = ConnectionMultiplexer.Connect(options);
            database = connection.GetDatabase();
        }

        public void Set(string key, string value)
        {
            database.StringSet(key, value, null, When.Always, CommandFlags.None);
        }

        public void Set(string key, string value, double secs)
        {
            database.StringSet(key, value, TimeSpan.FromSeconds(secs), When.Always, CommandFlags.None);
        }

        public void Set<T>(string key, T value, double secs)
        {
            using (var stream = new System.IO.MemoryStream())
            {
                var b = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                b.Serialize(stream, value);
                this.Set(key, stream.GetBuffer(), secs);
            }
        }

        public void Set(string key, byte[] value, double secs)
        {
            database.StringSet(key, value, TimeSpan.FromSeconds(secs), flags: CommandFlags.FireAndForget);
        }

        public T Get<T>(string key)
        {
            try
            {
                var r = database.StringGetWithExpiry(key, CommandFlags.PreferSlave);
                if (r.Expiry == TimeSpan.Zero || r.Value == RedisValue.Null)
                {
                    return default(T);
                }
                using (var memStream = new System.IO.MemoryStream(r.Value))
                {
                    return (T)new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Deserialize(memStream);
                }

            }
            catch (Exception ex)
            {
                //IBCFile.Logger.LogException(ex, "RedisStringsNew Error!!");
                return default(T);
            }
        }

        public string Get(string key)
        {
            //return base.Database.StringGet(key, CommandFlags.PreferSlave);
            try
            {
                var r = database.StringGetWithExpiry(key, CommandFlags.PreferSlave);
                if (r.Expiry == TimeSpan.Zero)
                {
                    return null;
                }
                return r.Value;
            }
            catch (Exception ex)
            {
                //IBCFile.Logger.LogException(ex, "RedisStringsNew Error!!");
                return null;
            }
        }

        public IDatabase GetDatabase()
        {
            return database;
        }

        public void Del(string key)
        {
            throw new NotImplementedException();
        }

        public void Expire(string key, int secs)
        {
            throw new NotImplementedException();
        }
    }

    public class RedisConnect
    {
        private RedisStringsNew redisStrings = null;
        public RedisConnect(string hosname)
        {
            redisStrings = new RedisStringsNew(hosname);
        }

        public void Set<T>(string key, T value, double secs)
        {
            redisStrings.Set<T>(key, value, secs);
        }

        public void Set(string key, byte[] value, double secs)
        {
            redisStrings.Set(key, value, secs);
        }

        public T Get<T>(string key)
        {
            return redisStrings.Get<T>(key);
        }

        public void Del(string RedisKey)
        {
            redisStrings.Del(RedisKey);
        }
        public IDatabase GetDatabase()
        {
            return redisStrings.GetDatabase();
        }
    }


    public class RedisUtil
    {
        private static IRedisStrings redisStrings = null;
        private static IRedisStrings redisStringsMy = null;
        private static IDatabase db = null;
        private static ConcurrentDictionary<string, Lazy<RedisConnect>> _CacheList = new ConcurrentDictionary<string, Lazy<RedisConnect>>();

        static RedisUtil()
        {
            try
            {
                //redisStrings = new NewRedisStrings(RedisSettings.GetOtherRedisHostName);
            }
            catch (Exception ex)
            {
                //IBCFile.Logger.LogException(ex, "RedisUtil Error!!");
                //throw ex;
            }
        }
        public static IRedisStrings GetStrings()
        {
            if (redisStrings == null)
            {
                try
                {
                    redisStrings = new NewRedisStrings("OtherRedis");
                }
                catch (Exception ex)
                {
                    //IBCFile.Logger.LogException(ex, "New Redis Error :" + ex.Message);
                }
            }
            return redisStrings;
        }

        public static IRedisStrings GetStringsMy()
        {
            if (redisStringsMy == null)
            {
                try
                {
                    redisStringsMy = new RedisStringsNew("127.0.0.1:6379");
                }
                catch (Exception ex)
                {
                    //IBCFile.Logger.LogException(ex, "New Redis Error :" + ex.Message);
                }
            }
            return redisStringsMy;
        }

        public static RedisConnect GetRedis(string Host)
        {
            Lazy<RedisConnect> redis = _CacheList.GetOrAdd(Host, (k) => new Lazy<RedisConnect>(() => new RedisConnect(k)));
            return redis.Value;
        }

        public static IDatabase GetDatabase()
        {
            if (db == null)
            {
                try
                {
                    var conn = ConnectionMultiplexer.Connect("127.0.0.1:6379");
                    db = conn.GetDatabase();
                }
                catch (Exception ex)
                {
                    //IBCFile.Logger.LogException(ex, "New Redis Error :" + ex.Message);
                }
            }
            return db;
        }

        public static void Set<T>(string key, T value, double secs)
        {
            RedisStringsNew redis = (RedisStringsNew)redisStrings;
            redis.Set<T>(key, value, secs);
        }

        public static T Get<T>(string key)
        {
            RedisStringsNew redis = (RedisStringsNew)redisStrings;
            return redis.Get<T>(key);
        }

        public static void SetDataToRedis(string RedisKey, string RedisValue, int ExpireTimeBySec)
        {
            try
            {
                RedisUtil.GetStrings().Set(RedisKey, RedisValue);
                RedisUtil.GetStrings().Expire(RedisKey, ExpireTimeBySec);
            }
            catch (Exception ex)
            {
                //IBCFile.Logger.LogException(ex, "Save Redis Key :" + RedisKey + " Redis Value:" + RedisValue + " to Redis Error!");
            }
        }

        public static void SetDataToRedis(string RedisKey, int ExpireTimeBySec)
        {
            try
            {
                RedisUtil.GetStrings().Expire(RedisKey, ExpireTimeBySec);
            }
            catch (Exception ex)
            {
                //IBCFile.Logger.LogException(ex, "Save Redis Key :" + RedisKey + " to Redis Error!");
            }
        }

        public static void DelDataToRedis(string RedisKey)
        {
            try
            {
                RedisUtil.GetStrings().Del(RedisKey);
            }
            catch (Exception ex)
            {
                //IBCFile.Logger.LogException(ex, "Del Redis Key :" + RedisKey + " to Redis Error!");
            }
        }
    }

    [Serializable]
    public class TestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

