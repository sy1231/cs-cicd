using WebApplication1.Models;

namespace WebApplication1.Repositories
{
    public interface IDeptRepository : IRepositoryBase<Dept>
    {

    }

    public interface IProjRepository : IRepositoryBase<Proj>
    {

    }

    public interface IUserRepository : IRepositoryBase<User>
    {

    }     
}