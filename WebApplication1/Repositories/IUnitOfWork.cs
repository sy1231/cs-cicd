using System;
using System.Data;
using System.Data.Entity;

namespace WebApplication1.Repositories
{
    public interface IUnitOfWork
    {
        IDeptRepository Dept { get; }
        IProjRepository Proj { get; }
        IUserRepository User { get; }
        void Save();
        DbContextTransaction Transaction();
    }
}