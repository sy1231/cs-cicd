using System;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;

namespace WebApplication1.Repositories
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> FindAll(bool tracking = false);
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, bool tracking = false);
        T Single(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteAll();        
        //IIncludableQueryable<T, TProperty> Include<TProperty>(Expression<Func<T, TProperty>> expression);
        IQueryable<T> Include<TProperty>(Expression<Func<T, TProperty>> expression);
        DbEntityEntry Entry(T entity);
    }
}