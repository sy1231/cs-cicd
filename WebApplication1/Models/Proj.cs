using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApplication1.Models
{
    [Table("proj")]
    public class Proj
    {
        public Proj()
        {
        }

        [JsonProperty(PropertyName = "id")]
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        [Column("name")]
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<UserRelProj> UserRelProjs { get; set; }
        // public virtual ICollection<User> Users { get; set; }
    }
}