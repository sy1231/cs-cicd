using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Web;

namespace WebApplication1.Models
{
    [Table("user")]
    public class User
    {
        public User()
        {
        }

        [JsonProperty(PropertyName = "id")]
        [Column("id")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        [Column("name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "hight")]
        [Column("hight")]
        public int Hight { get; set; }

        [JsonProperty(PropertyName = "birthday")]
        [Column("birthday")]
        [JsonConverter(typeof(OnlyDateConverter))]
        public DateTime Birthday { get; set; }

        [JsonProperty(PropertyName = "photo")]
        [Column("photo")]
        public string Photo { get; set; }

        [NotMapped]
        public HttpPostedFileBase PhotoFile { get; set; }

        [JsonProperty(PropertyName = "dept")] 
        [Column("dept_id")]
        [ForeignKey("Dept")]
        public int DeptId { get; set; }
        
        [JsonIgnore]
        public Dept Dept { get; set; }    

        [JsonIgnore]
        public ICollection<UserRelProj> UserRelProjs { get; set; }
        // public virtual ICollection<Proj> Projs { get; set; }

        [JsonProperty(PropertyName = "projs")]
        [NotMapped]
        public List<int> Projs { get; set; }
    }
}