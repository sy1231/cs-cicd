using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebApplication1.Models
{
    [Table("dept")]
    public class Dept
    {
        public Dept()
        {
        }

        [JsonProperty(PropertyName = "id")]
        [Column("id")]
        [Key]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        [Column("name")]
        public string Name { get; set; }

        //public virtual ICollection<User> Users { get; set; }
    }
}