using System.Data.Entity;
namespace WebApplication1.Models
{
    public class MyContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Dept> Depts { get; set; }
        public DbSet<Proj> Projs { get; set; }

        public MyContext() : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<UserRelProj>()
                .HasKey(t => new { t.UserId, t.ProjId });

            builder.Entity<UserRelProj>()
                .HasRequired(pt => pt.User)
                .WithMany(p => p.UserRelProjs)
                .HasForeignKey(pt => pt.UserId);

            builder.Entity<UserRelProj>()
                .HasRequired(pt => pt.Proj)
                .WithMany(t => t.UserRelProjs)
                .HasForeignKey(pt => pt.ProjId);

            base.OnModelCreating(builder);
        }
    }
}