﻿namespace WebApplication1.Models
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.dept",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 2147483647),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.user",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 2147483647),
                        hight = c.Int(nullable: false),
                        birthday = c.DateTime(nullable: false),
                        photo = c.String(maxLength: 2147483647),
                        dept_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.dept", t => t.dept_id, cascadeDelete: true)
                .Index(t => t.dept_id);
            
            CreateTable(
                "dbo.user_proj",
                c => new
                    {
                        user_id = c.Int(nullable: false),
                        proj_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.user_id, t.proj_id })
                .ForeignKey("dbo.proj", t => t.proj_id, cascadeDelete: true)
                .ForeignKey("dbo.user", t => t.user_id, cascadeDelete: true)
                .Index(t => t.user_id)
                .Index(t => t.proj_id);
            
            CreateTable(
                "dbo.proj",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 2147483647),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.user_proj", "user_id", "dbo.user");
            DropForeignKey("dbo.user_proj", "proj_id", "dbo.proj");
            DropForeignKey("dbo.user", "dept_id", "dbo.dept");
            DropIndex("dbo.user_proj", new[] { "proj_id" });
            DropIndex("dbo.user_proj", new[] { "user_id" });
            DropIndex("dbo.user", new[] { "dept_id" });
            DropTable("dbo.proj");
            DropTable("dbo.user_proj");
            DropTable("dbo.user");
            DropTable("dbo.dept");
        }
    }
}
