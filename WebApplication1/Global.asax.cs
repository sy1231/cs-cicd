﻿using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using WebApplication1.Models;
using WebApplication1.Repositories;
using WebApplication1.Services;

namespace WebApplication1
{    
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var container = new Container();
            container.Register<IUserRepository, UserRepository>(Lifestyle.Singleton);
            container.Register<IDeptRepository, DeptRepository>(Lifestyle.Singleton);
            container.Register<IProjRepository, ProjRepository>(Lifestyle.Singleton);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Singleton);
            container.Register<UserService, UserService>(Lifestyle.Singleton);
            container.Register<DeptService, DeptService>(Lifestyle.Singleton);
            container.Register<ProjService, ProjService>(Lifestyle.Singleton);
            container.Register<InitService, InitService>(Lifestyle.Singleton);
            container.Register<MyContext>(Lifestyle.Singleton);

            //MvcControllers
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.RegisterMvcIntegratedFilterProvider();
            //ControllerBuilder.Current.SetControllerFactory(new DiControllerFactory(container)); //公司的作法
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            //ApiControllers
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;

            //container.Verify(); //有error
        }

        protected void Application_PostAuthorizeRequest()
        {
            HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }
    }    
}
