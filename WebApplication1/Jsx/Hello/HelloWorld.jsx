import router from 'router';
// import React from "react";
// React在這專案可以不用import

class HelloWorldComponent extends React.Component {
    onClickItem() {
        router.navigateAndSaveLast(`#HiWorld/`, { trigger: true });
    }
    render() {
        return (
            <div>
                Hello World                   
                <button onClick={()=>this.onClickItem()}>hi</button>
                <button onClick={()=> window.location.href = "/#logout"}>logout</button>
            </div>            
        );
    }    
}

define([], function () {
    return HelloWorldComponent;
})