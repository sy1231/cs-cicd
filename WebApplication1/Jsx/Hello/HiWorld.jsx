import router from 'router'

function HiWorldHook() {
    return (
        <div>
            HI World
            <button onClick={()=>router.navigateAndSaveLast(`#HelloWorld`, { trigger: true })}>hello</button>
            <button onClick={()=> window.location.href = "Default"}>default</button>
        </div>            
    );
}

// class HiWorldComponent1 extends React.Component {
//     render() {
//         return (
//             <div>
//                 HI World！！！
//                 <button onClick={()=>router.navigateAndSaveLast(`#HelloWorld`, { trigger: true })}>hello</button>
//                 <button onClick={()=> window.location.href = "Default"}>default</button>
//             </div>            
//         );
//     }
// }

define([], function () {
    return  {
        Main: HiWorldHook
    }    
})