// import React from "react";
// import { Main as BaseComponent } from 'Commponent/Base';
//import BaseComponent from 'Commponent/Base'; //TODO 可能要用define
import router from 'router';
import common from 'common';


//TODO 只有第一次有用???
// const BASE_URL = `/api/user`;
// const IMG_URL = `/static`;

define(['Commponent/Base'], function (BaseComponent) {

  const BASE_URL = `/api/user`;
  const IMG_URL = `/static`;

  class UserList extends BaseComponent {
    constructor(props) {
      super(props);
      this.state = { list: [], keyWord: "" };
      this.getList = this.getList.bind(this);
      this.queryList = this.queryList.bind(this);
      // this.delete = this.delete.bind(this);
      // this.handleInputChange = this.handleInputChange.bind(this);
      // this.getList();
      console.log('it is UserList');
    }

    componentDidMount() {
      this.getList();
    }

    getList() {
      // let res = await UserService.get();
      // this.setState((state) => ({
      //   list: res.data,
      // }));
      common.SyncServer(`${BASE_URL}`, null, function (val) {
        this.setState((state) => ({
          list: val,
        }));
        console.log(val);
      }.bind(this), null, null, "GET");
    }

    queryList() {
      // let res = await UserService.getQuery(this.state.keyWord);
      // this.setState((state) => ({
      //   list: res.data,
      // }));
      common.SyncServer(`${BASE_URL}/query`, this.state.keyWord, function (val) {
        this.setState((state) => ({
          list: val,
        }));
      }.bind(this));
    }

    delete(id) {
      // let res = await UserService.delete(id);
      common.SyncServer(`${BASE_URL}/${id}`, null, function () {
        if (this.state.keyWord != "") {
          this.queryList();
        } else {
          this.queryList();
        }
      }.bind(this), null, null, "DELETE");
    }

    create() {
      this.props.history.push(`${BASE_URL}/0`);
    }

    render() {
      return (
        <div>
          <input
            name="keyWord"
            type="text"
            value={this.state.keyWord}
            onChange={this.handleInputChange}
          />
          <button onClick={this.queryList}>query</button>
          <br />
          <button onClick={() => this.create()}>create</button>
          <table className="table">
            <thead>
              <tr>
                <th>id</th>
                <th>name</th>
                <th>hight</th>
                <th>birthday</th>
                <th>photo</th>
                <th>edit</th>
                <th>del</th>
              </tr>
            </thead>
            <tbody>
              {this.state.list.map((item) => (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.hight}</td>
                  <td>{item.birthday}</td>
                  <td>
                    {item.photo && (
                      <img height="50" src={`${IMG_URL}/img/${item.photo}`} />
                    )}
                  </td>
                  <td>
                    {/* <Link to={`/${rootPath}/${item.id}`}>read</Link> */}
                    <button onClick={() => router.navigateAndSaveLast(`#User/id=${item.id}`, { trigger: true })}>read</button>
                  </td>
                  <td>
                    <button onClick={() => this.delete(item.id)}>del</button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
    }
  }

  return UserList;
})
