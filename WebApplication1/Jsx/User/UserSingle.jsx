//import React from "react";
import router from "router";
// import { Main as BaseComponent } from 'Commponent/Base';
import common from 'common';


const BASE_URL = `/api`;
const USER_URL = `${BASE_URL}/user`;
const IMG_URL = `/static`;
function SelectList(props) {
  return (
    <select name={props.name} value={props.value} onChange={props.onChange}>
      <option value="">請選擇</option>
      {props.list &&
        props.list.map((item) => (
          <option value={item.value}>{item.name}</option>
        ))}
    </select>
  );
}

define(['Commponent/Base'], function (BaseComponent) {
  class UserSingle extends BaseComponent {
    constructor(props) {
      super(props);
      this.state = {
        row: {
          // id: null,
          name: null,
          hight: null,
          dept: 0,
          projs: null,
          photo: null,
          birthday: null,
        },
        photo: null,
        photoFile: null,
      };
      this.selectProjAll = this.selectProjAll.bind(this);
      this.changeProj = this.changeProj.bind(this);
      this.getFile = this.getFile.bind(this);
      this.read = this.read.bind(this);
      if (this.props.id > 0) {
        this.read(this.props.id);
      }
    }
    // componentDidMount() {
    //   if (this.props.id > 0) {
    //     this.read(this.props.id);
    //   }
    // }

    read(id) {
      // UserService.getSingle(id).then((m) => {
      //   this.setState((state) => ({
      //     row: m.data,
      //     photo: m.data.photo,
      //   }));
      // });
      common.SyncServer(`${USER_URL}/${id}`, null, function (val) {
        this.setState((state) => ({
          row: val,
          photo: val.photo,
        }));
      }.bind(this), null, null, "GET");
    }

    selectProjAll(e) {
      let checked = e.target.checked;
      let projs = [];
      if (checked) {
        projs = this.props.projs.map((m) => m.value);
      }
      let row = this.state.row;
      row.projs = projs;
      this.setState((state) => ({
        row: row,
      }));
    }

    changeProj(e) {
      let projs = [];
      if (this.state.row.projs) {
        projs = [...this.state.row.projs];
      }

      let v = parseInt(e.target.value);
      let index = projs.indexOf(v);
      if (e.target.checked) {
        if (index < 0) {
          projs.push(v);
        }
      } else {
        if (index > -1) {
          projs.splice(index, 1);
        }
      }

      let row = this.state.row;
      row.projs = projs;
      this.setState((state) => ({
        row: row,
      }));
    }

    save() {
      let row = this.state.row;
      row.id = parseInt(this.props.id);

      if (this.state.photoFile) {
        // await UserService.upload(this.state.photoFile).then((m) => {
        //   row.photo = m.data;
        // });
        const fData = new FormData();
        if (data != null) {
          fData.append("file", this.state.photoFile);
        }
        $.ajax({
          url: `${USER_URL}/ufile`,
          data: fData,
          method: "POST",
          headers: {
            "Content-Type": "multipart/form-data",
          },
          success: function (result) {
            row.photo = result;
          }
        });
      }

      if (row.id > 0) {
        //await UserService.put(row);
        //common.SyncServer(`${USER_URL}/${row.id}`, row, null, null, null, "PUT",null,null, "application/json");
        $.ajax({
          url: `${USER_URL}/${row.id}`,
          data: row,
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          success: function (result) {
            row.photo = result;
          }
        });
      } else {
        // await UserService.post(row);
        common.SyncServer(`${USER_URL}`, row, null, null, null);
      }
      this.props.onBack();
    }

    getFile(e) {
      //let files:FileList = e.target.value;
      let files = e.target.files;
      this.setState((state) => ({
        photoFile: files[0],
      }));
    }

    render() {
      return (
        <div>
          <table>
            <tbody>
              <tr>
                <th>id</th>
                <td>{this.props.id > 0 && this.props.id}</td>
              </tr>
              <tr>
                <th>name</th>
                <td>
                  <input
                    name="row.name"
                    type="text"
                    value={this.state.row.name}
                    onChange={this.handleInputChange}
                  />
                </td>
              </tr>
              <tr>
                <th>hight</th>
                <td>
                  <input
                    name="row.hight"
                    type="number"
                    value={this.state.row.hight}
                    onChange={this.handleInputChange}
                  />
                </td>
              </tr>
              <tr>
                <th>birthday</th>
                <td>
                  <input
                    name="row.birthday"
                    type="date"
                    value={this.state.row.birthday}
                    onChange={this.handleInputChange}
                  />
                </td>
              </tr>
              <tr>
                <th>dept</th>
                <td>
                  <SelectList
                    name="row.dept"
                    list={this.props.depts}
                    value={this.state.row.dept}
                    onChange={this.handleSelectNumberChange}
                  />
                </td>
              </tr>
              <tr>
                <th>
                  proj
                  <input type="checkbox" onChange={this.selectProjAll} />
                </th>
                <td>
                  {this.props.projs &&
                    this.props.projs.map((item) => (
                      <span>
                        <label>
                          <input
                            type="checkbox"
                            onChange={this.changeProj}
                            checked={
                              this.state.row.projs &&
                              -1 !== this.state.row.projs.indexOf(item.value)
                            }
                            value={item.value}
                          />
                          {item.name}
                        </label>
                      </span>
                    ))}
                </td>
              </tr>
              <tr>
                <th>photo</th>
                <td>
                  {this.state.photo && (
                    <img
                      height="200"
                      src={`${IMG_URL}/img/${this.state.photo}`}
                    />
                  )}
                  <input type="file" onChange={this.getFile} />
                </td>
              </tr>
            </tbody>
          </table>
          <button onClick={() => this.save()}>save</button>
          <button onClick={() => this.props.onBack()}>back</button>
        </div>
      );
    }
  }

  class UserSingleDisplay extends React.Component {
    constructor(props) {
      super(props);
      this.state = { depts: null, projs: null };
      this.onBack = this.onBack.bind(this);
      // (async () => {
      //   let depts = await DeptService.get();
      //   this.setState({
      //     depts: depts.data.map((m) => {
      //       let item = {
      //         value: m.id,
      //         name: m.name,
      //       };
      //       return item;
      //     }),
      //   });

      //   let projs = await ProjService.get();
      //   this.setState({
      //     projs: projs.data.map((m) => {
      //       let item = {
      //         value: m.id,
      //         name: m.name,
      //         checked: false,
      //       };
      //       return item;
      //     }),
      //   });
      // })();
    }

    componentDidMount(){
      common.SyncServer(`${BASE_URL}/dept`, null, function (val) {   
        this.setState({
          depts: val.map((m) => {
            let item = {
              value: m.id,
              name: m.name,
            };
            return item;
          }),
        });        
      }.bind(this), false, null, "GET");

      common.SyncServer(`${BASE_URL}/proj`, null, function (val) {
        this.setState({
          projs: val.map((m) => {
            let item = {
              value: m.id,
              name: m.name,
            };
            return item;
          }),
        });
      }.bind(this), false, null, "GET");
    }

    onBack() {
      router.navigateAndSaveLast(`#User`, { trigger: true })

      // this.props.history.goBack();
      //return <Redirect to={"/" + rootPath} />;
      // return <Redirect to="/user" />;
    }

    render() {
      const id = this.props.id;
      return (
        <UserSingle
          id={id}
          depts={this.state.depts}
          projs={this.state.projs}
          onBack={this.onBack}
        />
      );
    }
  }

  return UserSingleDisplay;

})