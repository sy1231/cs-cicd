define(['react','router','react.backbone'], function (React, router) {
    var Main = React.createBackboneClass({
        render: function () {
            return (
                <div>
                    HI Define
                    <button onClick={()=>router.navigateAndSaveLast(`#HelloDefine`, { trigger: true })}>hello</button>
                    <button onClick={()=> window.location.href = "Default"}>default</button>
                </div>            
            );
        }
    });

    return {Main: Main}
});