define(['react','router','react.backbone'], function (React, router) {
    var Main = React.createBackboneClass({
        onClickItem: function () {
            router.navigateAndSaveLast(`#HiDefine`, { trigger: true });
        },
        render: function () {
            return (
                <div>
                    Hello Define                    
                    <button onClick={()=>this.onClickItem()}>hi</button>
                    <button onClick={()=> window.location.href = "/#logout"}>logout</button>
                </div>            
            );
        }
    });

    return {Main: Main}
});