using System.IO;
using System.Web;

namespace WebApplication1.Services
{
    public class FileService
    {
        public static string Upload(HttpPostedFileBase file, string folder)
        {
            var folderPath = Path.Combine("Static", folder);

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            } 

            var filePath = Path.Combine(
                                Path.Combine(Directory.GetCurrentDirectory(), folderPath),
                                file.FileName);
                                               
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            file.SaveAs(filePath);

            return file.FileName;
        }

        public static void Clear(string folder){
            var folderPath = Path.Combine("Static", folder);

            if (!Directory.Exists(folderPath)){
                return;
            }

            var dir = new DirectoryInfo(
                Path.Combine(
                    Path.Combine(
                        Directory.GetCurrentDirectory(), folderPath))                
                );

            foreach (var f in dir.GetFiles())
            {
                f.Delete(); 
            }
            foreach (var d in dir.GetDirectories())
            {
                d.Delete(true); 
            }
        }
    }
}