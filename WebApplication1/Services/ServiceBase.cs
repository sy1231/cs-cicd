using WebApplication1.Repositories;

namespace WebApplication1.Services
{
    public class ServiceBase
    {
        protected readonly IUnitOfWork _unitofwork;

        public ServiceBase(IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
        }        
    }
}