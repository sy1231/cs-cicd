﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching; //如果放在App_Code底下會無法參考
namespace WebApplication1.Commons
{
    public class Caching
    {
        public static string Get(string key)
        {
            ObjectCache objCache = MemoryCache.Default;
            objCache.Set("time", System.DateTime.Now.ToString(), new CacheItemPolicy());
             var res = objCache["time"] as string;
            return res;
        }

        public static void Set(string key, string value)
        {
            ObjectCache objCache = MemoryCache.Default;
            objCache.Set(key, value, new CacheItemPolicy());
        }
    }
}