require('es6-promise').polyfill()
var webpack=require("webpack")
process.env.NODE_ENV="production";
module.exports = {
  entry: {
    main:'./WebApplication1/Scripts/main',
  },
  output: { 
	  path: 'WebApplication1/bundles/',
    publicPath:'bundles/',
    filename: '[name].js' // Template based on keys in entry above
  },
  module: {
    loaders: [
//      { test: /\.js$/, loader: 'babel-loader' },
//      { test: /\.less$/, loader: 'style-loader!css-loader!less-loader' }, // use ! to chain loaders
//      { test: /\.css$/, loader: 'style-loader!css-loader' }
//      { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' } // inline base64 URLs for <=8k images, direct URLs for the rest
      { test: /\.(png|jpg|svg|woff|woff2|ttf|eot)(\?.*)?$/, loader: 'file-loader' } // inline base64 URLs for <=8k images, direct URLs for the rest
    ]
  },
	plugins: [
//		new webpack.optimize.CommonsChunkPlugin("common","common.js"),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
	],
  resolve: {
    modulesDirectories:["node_modules","Scripts","Commponent","Content"],
    extensions: ['', '.js','.css'],
    alias:{
      //"underscore": "underscore-min.js",
      //"jquery":"jquery-2.1.3.min.js",
		//	"backbone":"backbone-min.js",			
		//	"react":"react/react.min.js",
		//	"react.backbone":"react.backbone.js",
		//	"signalr.core": "jquery.signalR-2.2.0.min"
    }
  },
  devtool:"source-map",
  externals :{"jquery":"jQuery","backbone":"Backbone","underscore":"_"},
};