var gulp = require("gulp");
var gutil = require("gulp-util");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var replace = require('gulp-replace-task')
var args = require('yargs').argv
var fs = require('fs')
//handleErrors=require("handleErrors"),
//$ = require("gulp-load-plugins")();  /* https://www.npmjs.com/package/gulp-load-plugins */

var builds = function (url, env) {
    var start = Date.now();

    var arr = [];

    let path = "";
    const _url = url.split("Jsx\\");
    if (_url.length == 2) path = _url[1].replace("\\", "/").replace(/[\w]+.jsx/g, "");

    gulp.src(url)
        .pipe(replace({
            patterns: arr
        }))
        .pipe(sourcemaps.init())
        .pipe(babel({
            plugins: ["react-display-name"],
            blacklist: ["useStrict"]
        }))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("WebApplication1/Scripts/Commponent/" + path));
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "/" +
        (currentdate.getMonth() + 1) + "/" +
        currentdate.getDate() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();


    console.log(datetime + ', built in ' + (Date.now() - start) + 'ms');
};

gulp.task("watch", function (e) {
    var env = args.env || 'prod';
    console.log("watch", env)
    builds("WebApplication1/Jsx/**/*.jsx", env);
    gulp.watch("WebApplication1/Jsx/**/*.jsx").on("change", function (e) {
        console.log("file:" + e.path + " was " + e.type);
        builds(e.path, env);

    })
});



gulp.task("default", ["watch"]);


//= webpack ========================
var webpack = require("webpack");
var wp_config = require("./webpack.config");

gulp.task("webpack",/*["build-react"],*/ function (callback) {
    // run webpack
    webpack(wp_config, function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        callback();
    });
});

//= amd-optimize ==================

var amdOptimize = require("amd-optimize");
var concat = require('gulp-concat');


gulp.task("amd", function () {
    var amdOptions = {
        paths: {
            "backbone": "./backbone-min",
            "underscore": "./underscore-min",
            "jquery": "./jquery-2.1.3.min"
        }
    }
    return gulp.src(["WebApplication1/Scripts/**/*.js"])
        .pipe(sourcemaps.init())
        //        .pipe(babel( {plugins: ["react-display-name"]}))
        // Traces all modules and outputs them in the correct order. 
        .pipe(amdOptimize("main", amdOptions))
        .pipe(concat("amdBundle.js"))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("WebApplication1/bundles/"));
});

//= requirejs-optimize ==================

var requirejsOptimize = require('gulp-requirejs-optimize');
gulp.task("rop", function () {
    var options = {
        mainConfigFile: './WebApplication1/Scripts/main.js',
    }
    return gulp.src([
        "./WebApplication1/Scripts/main.js",
        "./WebApplication1/Scripts/lobbyModular.js"
    ])
        .pipe(sourcemaps.init())
        .pipe(requirejsOptimize(options))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("WebApplication1/bundles/"));
});

//============ localhost test
var fs = require('fs');
var webserver = require('gulp-webserver');

//============

var buildDev = function (url, env) {
    var start = Date.now();
    // Read the settings from the right file
    var filename = './env/config.' + env + '.json';
    var settings = JSON.parse(fs.readFileSync('./' + filename, 'utf8'));
    var arr = [];
    for (var prop in settings) {
        arr.push({
            match: prop,
            replacement: settings[prop]
        });
    }
    console.log('settings , ', arr, settings);
    gulp.src(url)
        .pipe(replace({
            patterns: arr
        }))
        .pipe(sourcemaps.init())
        .pipe(babel({
            plugins: ["react-display-name"],
            blacklist: ["useStrict"]
        }))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("./html/Scripts/Commponent/"));
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "/" +
        (currentdate.getMonth() + 1) + "/" +
        currentdate.getDate() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();


    console.log(datetime + ', built in ' + (Date.now() - start) + 'ms');
};

gulp.task("dev", ["gulp-dev"]);
gulp.task("gulp-dev", () => {
    var env = args.env || 'poc';
    console.log(args);

    buildDev("WebApplication1/Jsx/**/*.jsx", env);
    gulp.watch("WebApplication1/Jsx/**/*.jsx").on("change", function (e) {
        console.log("file:" + e.path + " was " + e.type);
        buildDev(e.path, env);
    });
    gulp.src('./html')
        .pipe(webserver({
            livereload: true,
            open: true,
            host: 'localhost',
            port: 10000,
            middleware: function (req, res, next) {
                var proxies = require('./html/json/proxies.json');
                let path = req.originalUrl.split('?')[0];

                if (path in proxies) {

                    let filename = `./html/json/${proxies[path]}`;
                    try {
                        fs.accessSync(filename, fs.constants.R_OK);
                        res.setHeader('Content-Type', 'application/json');
                        var data = fs.readFileSync(filename, 'utf-8');
                        res.end(data);
                    } catch (err) {
                        res.setHeader('Content-Type', 'application/json');
                        res.end(JSON.stringify({
                            "status": "no file",
                            "notFound": filename
                        }));
                    }
                } else {
                    next();
                }
            }
        }));
});

//var gulp = require('gulp'),
//    browserSync = require('browser-sync').create()


gulp.task("webdev", ["webgulp-dev"]);
gulp.task("webgulp-dev", () => {
    var env = args.env || 'uat';
    builds("WebApplication1/Jsx/**/*.jsx", env)
    gulp.watch("WebApplication1/Jsx/**/*.jsx").on("change", function (e) {
        console.log("file:" + e.path + " was " + e.type);
        builds(e.path, env);
    });
    gulp.src('./WebApplication1')
        .pipe(webserver({
            livereload: true,
            open: false,
            host: 'localhost',
            port: 10000,
            middleware: function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                let path = req.originalUrl.split('?')[0];
                console.log(path);
                next();
            }

        }));
});